#include <bits/stdc++.h>
using namespace std;
typedef long double ld;

#define PI acos(-1)
#define EPS 1e-7

struct P {
	ld x, y;
	P(){x=y=0.0;}
	P(ld _x, ld _y) {
		x = _x, y = _y;
	}
};

//ld Dist(P p1, P p2) { return hypot(p1.x - p2.x, p1.y - p2.y);	}

struct PointP{
	ld dis, ang, den, num;
	P p; int j;
	PointP(){
		dis=0.0; ang=0.0; p=P(0,0);
	}
	PointP(P a, P c, int _j){		
		dis=hypot(c.x - a.x, c.y - a.y);	
		p=a;
		ld x=a.x-c.x, y=a.y-c.y;
		ang=0.0;
		if(x>0.0 and y>=0.0) ang=atan(y/x); //ang=atan2(y,x);
		if(x==0.0 and y>0.0) ang=PI/2.0;
		if(x<0.0) ang=atan(y/x)+PI; //ang=atan2(y,x)+PI;
		if(x==0.0 and y<0.0) ang=(3*PI)/2.0;
		if(x>0.0 and y<0.0) ang=atan(y/x)+2*PI; //ang=atan2(y,x)+2*PI;
		//den = a.y-c.y; //num = hypot(a.x-c.x, a.y-c.y);	//ang = den/num;
		j=_j;
	}
	PointP(ld _dis, ld _ang){
		dis=_dis;
		ang=_ang;
	}
};

bool di(ld a, ld b){ return fabsl(a-b)>=EPS; }
bool CompAng(PointP a, PointP b){
	if(di(a.ang,b.ang))
		return a.ang < b.ang;
	return a.dis < b.dis;
}

P K[10];
vector<PointP> Vp[10];
int main(){
	int k, n;
	while(scanf("%d %d\n", &k, &n)!=EOF){
		for(int i=0; i<k ;i++){
			cin>>K[i].x>>K[i].y;
			Vp[i].clear();
		}
		ld a, b;
		for(int i=0; i<n; i++){
			cin>>a>>b;
			for(int j=0; j<k; j++){
				Vp[j].push_back(PointP(P(a, b), K[j], i));
				cout<<Vp[j][i].p.x<<","<<Vp[j][i].p.y<<"  ";
			}//cout<<endl;
		}
		for(int i=0; i<k; i++){
			sort(Vp[i].begin(), Vp[i].end(), CompAng);
			cout<<"I:"<<i<<" "<<K[i].x<<" "<<K[i].y<<" "<<endl;
			for(int j=0; j<n; j++) cout<<Vp[i][j].ang<<","<<Vp[i][j].p.x<<","<<Vp[i][j].p.y<<" "<<Vp[i][j].j<<"   "; cout<<endl;
		}
		bool A[1100];
		int P[7]; for(int i=0; i<7; i++) P[i]=i;
		int Mx=0;
		do{
			//for(int i=0; i<k; i++) cout<<P[i]<<" "; cout<<endl;
			memset(A, 0, n);
			for(int i=0; i<k; i++){
				int I=P[i];
				ld ang=100000.0;
				for(int j=0; j<n; j++){
					if(A[ Vp[I][j].j ]) continue;
					if(di(ang, Vp[I][j].ang) or ang==100000.0){
						ang=Vp[I][j].ang; A[ Vp[I][j].j ]=1;
						//cout<<"m:"<<Vp[I][j].j<<" ";
					}
				}//cout<<endl;
			}
			//for(int i=0; i<n; i++){cout<<A[i]<<" ";} cout<<endl;
			int cnt=0;
			for(int i=0; i<n; i++){
				cnt+=A[i];
			}
			//cout<<cnt<<endl;
			Mx=max(Mx, cnt);
		}while(next_permutation(P, P+k));
		printf("%d\n", Mx);
	}
	return 0;
}
