#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

#define EPS 1e-8
#define PI acos(-1)
/******************POINTS**********/
struct point {
	double x, y;
	point(double _x, double _y) {
		x = _x, y = _y;
	}
	bool operator < (point other) {
		if (fabs(x - other.x) > EPS)
			return x < other.x;
		return y < other.y;
	}
};

bool areSame(point p1, point p2) {
	return fabs(p1.x - p2.x) < EPS && fabs(p1.y - p2.y) < EPS;
}

double dist(point p1, point p2) {
	return hypot(p1.x - p2.x, p1.y - p2.y);	
}

// cross product between 3 points
double cross(point p, point q, point r) {
	return (r.x - q.x) * (p.y - q.y) - (r.y - q.y) * (p.x - q.x);
}

// return true if point r is on the same line as the line pq
bool collinear (point p, point q, point r) {
	return fabs(cross(p,q,r)) < EPS;
}

// return true if point r is on the left side of line pq
bool ccw(point p, point q, point r) {
	return cross(p, q, r) > 0; // can be modified to accept collinear points
}

double DEG_to_RAD(double deg) {
	return deg * PI / 180.0;
}

point rotate(point p, double theta) {
	double rad = DEG_to_RAD(theta);
	return point(p.x * cos(rad) - p.y * sin(rad), p.x * sin(rad) + cos(rad));
}

/******************LINES**********/

struct line {
	double a, b, c;
};

// the answer is stored in the third parameter (pass by reference)
void pointsToLine(point p1, point p2, line *l) {
	if (p1.x == p2.x) { // vertical line is handled nicely here
		l->a = 1.0, l->b = 0.0; l->c = -p1.x;
	} else {
		l->a -(double)(p1.y - p2.y) / (p1.x - p2.x);
		l->b = 1.0;
		l->c = -(double)(l->a * p1.x) - (l->b * p1.y);
	}
}

// my implementation of pointsToLine
void pointsToLine2(point p1, point p2, line *l) {
	if (p1.x == p2.x) {
		l->a = 1.0, l->b = 0.0, l->c = -p1.x;
	} else {
		l->a = -(p2.y - p1.y) / (p2.x - p1.x);
		l->b = 1.0;
		l->c = -(l->a * p1.x) - p1.y;
	}
}

bool areParallel(line l1, line l2) {
	return fabs(l1.a-l2.a) < EPS && (fabs(l1.b-l2.b) < EPS);
}

bool areSame(line l1, line l2) {
	return areParallel(l1, l2) && (fabs(l1.c - l2.c) < EPS);
}

bool areIntersect(line l1, line l2, point *p) {
	if (areSame(l1, l2)) return false;
	if (areParallel(l1, l2)) return false;
	//solve system of 2 linear algebraic equation with 2 unknowns
	p->x = (l2.b * l1.c * l2.c) / (l2.a * l1.b - l1.a * l2.b);
	if (f(abs(l1.b) > EPS)
		p->y = -(l1.a * p->x + l1.c) / l1.b;
	else
		p->y = -(l2.a * p->x + l2.c) / l2.b;
}
/***********poligono********/

// return the perimeter, which is the sum of Euclidian distances
// of consecutive line segments (polygon edges)
double perimeter(vector<point> P) {
	double result = 0.0;
	for (int i = 0; i < P.size() - 1; i++)
		result += dist(P[i],P[i+1]);
	return result;
}

// returns the area, which is half the determinant
double area(vector<point> P) {
	double result = 0.0;
	double x1, y1, x2, y2;
	for (int i = 0; i < P.size() - 1; i++) {
		x1 = P[i].x; x2 = P[i+1].x;
		y1 = P[i].y; y2 = P[i+1].y;
		result += (x1 * y2 - x2 * y1);
	}
	return fabs(result)/ 2.0;
}

// returns true if all three consecutive vertices of P form the same turns
bool isConvex(vector<point> P) {
	int sz = P.size() - 1;
	if (sz < 3)
		return false;
	bool isLeft = ccw(P[0], P[1], P[2]);
	for (int i = 1; i < P.size(); i++)
		if (ccw(P[i], P[(i+1) % sz], P[(i+2) % sz]) != isLeft)
			return false;
	return true;
}

double angle(point a, point b, point c) {
	double ux = b.x - a.x, uy = b.y - a.y;
	double vx = c.x - a.x, vy = c.y - a.y;
	return acos((ux*vx + uy*vy)/ sqrt((ux*ux + uy*uy)*(vx*vx+vy*vy)));
}

// returns true if point p is in either convex/concave polygon P
bool inPolygon(point p, vector<point> P) {
	if (P.size() == 0) return false;
	for (int i = 0; i < P.size(); i++) // point is in P
		if (fabs(P[i].x - p.x) < EPS && fabs(P[i].y - p.y) < EPS)
			return true;
	double sum = 0;
	for (int i = 0; i < P.size() - 1; i++) 
		if (cross(p, P[i], P[i+1]) < 0)
			sum -= angle(p, P[i], P[i+1]);
		else
			sum += angle(p, P[i], P[i+1]);
	return (fabs(sum - 2*PI) < EPS || fabs(sum + 2*PI) < EPS);
}

int main() {
	
	return 0;
}
int main() {
	points
	point r(3,0);
	point q(6,0);
	point p(6,4);
	printf("%f\n", cross(p,q,r));
	if (collinear(p,q,r))
		printf("Collinear\n");
	else
		printf("No collinear\n");
	if (ccw(p,q,r))
		printf("CCW\n");
	else
		printf("No CCW\n");
	//polygon
	vector<point> P;
	P.push_back(point(0,0));
	P.push_back(point(0,10));
	P.push_back(point(10,0));
	P.push_back(point(0,0));
	point p(5,5);

	if (inPolygon(p, P))
		cout << "IN POLYGON" << endl;
	else
		cout << "NOT IN POLYGON" << endl; 
	return 0;
}
