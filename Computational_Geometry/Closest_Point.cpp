#include <bits/stdc++.h>
using namespace std;
#define y second
#define x first
typedef long long ll;
typedef long double ld;
typedef pair<ll,ll> pt;
vector<pt> A;
set<pt>S;
ld dist(pt a, pt b){
	ld u = (ld)(a.x-b.x);
	ld v = (ld)(a.y-b.y);
	return sqrtl(u*u+v*v);
}
ld MinDist(){
	int n=A.size();
	S.clear();
	sort(A.begin(),A.end());
	ld d=100000;
	set<pt>::iterator it;
	S.insert(A[0]);
	int j=0;
	pt p,q;
	for(int i=1;i<n;i++){
		while(j<i and A[i].x-A[j].x>d)
			S.erase(A[j++]);
		for(it=S.lower_bound(pt(A[i].x-d,A[i].y-d));it!=S.end() and (*it).x<=A[i].x+d;it++)
			if(dist(*it,A[i])<d){
				d=dist(*it,A[i]);
				p=*it;
				q=A[i];
			}
		S.insert(A[i]);
	}
	return d;
}
int main(){
	int n;
	while(cin>>n){
		if(!n)break;
		A.assign(n,pt());
		for(int i=0;i<n;i++)
			cin>>A[i].x>>A[i].y;
		long double dist = MinDist();
		if(dist<10000.0)
			printf("%.4f\n",(double)dist);
		else printf("%s\n","INFINITY");
	}
	return 0;
}
