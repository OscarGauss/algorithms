#include <bits/stdc++.h>

using namespace std;

struct Point {
	double x, y;
	Point(){
		x=y=0.0;
	}
	Point(double _x, double _y) {
		x = _x, y = _y;
	}
		bool operator == (Point A){
		if(A.x==x and A.y==y)
			return true;
		return false;
	}
	bool operator != (Point A){
		if(A.x==x and A.y==y)
			return false;
		return true;
	}
};


struct Line{
	double a, b, c;
	Line(){
		a=b=c=0;
	}
	Line(double _a, double _b, double _c){
		a=_a; b=_b; c=_c;
	}
	Line(Point A, Point B){
		if(A.x == B.x){
			a=1.0; b=0.0; c=-A.x;
		}else{
			a=-(A.y-B.y)/(A.x-B.x);
			b=1.0;
			c=-(a*A.x)-A.y;
		}
	}
};

struct Circle{//(x-a)^2+(y-b)^2=r^2
	Point C;
	double r;
	Circle(){
		C.x=C.y=r=0.0;
	}
	Circle(double _a, double _b, double _r){
		C.x=_a; C.y=_b; r=_r;
	}
	bool operator == (Circle A){
		if(A.C==C and A.r==r)
			return true;
		return false;
	}
};

void Print(Point a){
	printf("(%lf, %lf)", a.x, a.y);
}

pair<double, double> EcuCua(double A, double B, double C){
	double Discri=B*B-4.0*A*C;
	if(Discri<0)
		return make_pair(0.0, 0.0);
	return make_pair((-B+sqrt(Discri))/(2*A), (-B-sqrt(Discri))/(2*A));
}
pair<Point, Point> Intersection(Circle A, Line B){
	pair<double, double> X, Y;
	double Aux=A.C.x*A.C.x+A.C.y*A.C.y-A.r*A.r;
	if(B.b!=0){//reemplazar x
		X=EcuCua(1.0+(B.a/B.b)*(B.a/B.b), 2.0*((B.a/B.b)*(B.c/B.b+A.C.y)-A.C.x), Aux+(B.c/B.b)*(B.c/B.b)+(2.0*B.c*A.C.y)/B.b);
		return make_pair(Point(X.first, (-B.c-B.a*X.first)/B.b), Point(X.second, (-B.c-B.a*X.second)/B.b));
	}else{
		if(B.a!=0){
			Y=EcuCua(1.0+(B.b/B.a)*(B.b/B.a), 2.0*((B.b/B.a)*(B.c/B.a+A.C.x)-A.C.y), Aux+(B.c/B.a)*(B.c/B.a)+(2.0*B.c*A.C.x)/B.a);
			return make_pair(Point((-B.b*Y.first-B.c)/B.a, Y.first), Point((-B.b*Y.second-B.c)/B.a, Y.second));			
		}
	}
}

pair<Point, Point> Intersection(Circle A, Circle B){
	double d=hypot(A.C.x-B.C.x, A.C.y-B.C.y);
	double w, z;
	pair<double, double> Y;
	if(fabs(A.r-B.r)<=d and d<=A.r+B.r and !(A==B)){
//		w=(B.C.y-A.C.y)/(B.C.x-A.C.x);
//		z=(A.C.x*A.C.x + A.C.y*A.C.y - B.C.x*B.C.x - B.C.y*B.C.y + A.r*A.r - B.r*B.r)/(2.0*(B.C.x-A.C.x));
//		Y=EcuCua(w*w+1, w*z+2*w*A.C.x-2*A.C.y, )
//		return Intersection(A, Line(1, w, z));
		return Intersection(A, Line(2*(B.C.x-A.C.x), 2*(B.C.y-A.C.y), A.C.x*A.C.x+A.C.y*A.C.y-B.C.x*B.C.x-B.C.y*B.C.y-A.r*A.r+B.r*B.r) );
	}
	return make_pair(Point(), Point());
}

int main(){
	int A, B, R, AA, BB, RR;
	while(scanf("%d %d %d %d %d %d", &A, &B, &R, &AA, &BB, &RR)!=EOF){
		pair<Point, Point> Aux=Intersection(Circle(A, B, R), Circle(AA, BB, RR));
		Print(Aux.first);cout<<endl;
		Print(Aux.second);cout<<endl;
	}
	return 0;
}
