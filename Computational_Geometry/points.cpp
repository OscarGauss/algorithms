#include <bits/stdc++.h>

using namespace std;
typedef long long ll;
typedef long double ld;

const ld EPS = 1e-8;
const ld PI = acos(-1);

struct point {
  ld x, y;
  int id;
  point() { x = y = 0.0; }
  point(ld a, ld b) { x = a; y = b; }
  point operator+(point a) { return point(x + a.x, y + a.y); }
  point operator-(point a) { return point(x - a.x, y - a.y); }
  ld operator*(point a) { return x * a.x + y * a.y; }
  point operator*(ld k) { return point(x * k, y * k); }
  point operator/(ld k) { return point(x / k, y / k); }
  ld operator^(point a) { return x * a.y - a.x * y; }
	ld operator|(point v) { return (*this - v).mod(); }   // distancia
  ld operator||(point v) { return (*this - v).mod2(); } // distancia2
  bool operator<(point v) {
    if (fabs(x - v.x) > EPS) return x < v.x;
    return y < v.y;
  }
  bool operator==(point v) {
    return (fabs(x - v.x) <= EPS and fabs(y - v.y) <= EPS);
  }
  bool operator!=(point v) { return !(*this == v); }
  ld mod() { return sqrtl(mod2()); }     // modulo
  ld mod2() { return *this * *this; }    // modulo
  point unit() { return *this / mod(); } // unitario
  point ort() { return point(-y, x); }   // ortogonal
  point operator%(ld a) {                // rotar antihorario
    return point(x * cos(a) - y * sin(a), x * sin(a) + y * cos(a));
  }
  ld ang() {
    ld ans = atan2(y, x);
    if (ans < 0) ans += 2.0 * PI;
    return ans;
  }
  ld ang(point v) {
    ld ans = v.ang() - ang();
    if (ans < 0) ans += 2.0 * PI;
    return ans;
  }
  ld ang(point b, point c) { // angulo antihorario
    return (b - *this).ang(c - *this);
  }
  point proy(point b, point c) { // proyeccion de c en ab
    point a = *this, u = (b - a), v = (c - a);
    return a + (u * ((u * v) / (u * u)));
  }
  int ori(point b, point c) { // 0, -1, 1
    point a = *this;
    ld aux = (b - a) ^ (c - a);
    if (fabs(aux) <= EPS) return 0;
    return (aux < 0 ? -1 : 1);
  }
  bool isCol(point b, point c) { // colinial thisab
    return ori(b, c) == 0;
  }
  bool onSeg(point a, point b) { // si this esta en el segmento ab
    return (max(*this | a, *this | b) <= (a | b) + EPS and isCol(a, b));
  }
};

ostream &operator<<(ostream &os, point a) {
  os << "(" << a.x << "," << a.y << ")";
  return os;
}

struct circle {
  point pc;
  ld r;
  circle() { pc = point(); r = 0; }
  circle(point p, ld _r) { pc = p; r = _r; }
  vector<point> pntsTanExt(circle v) {
    circle u = *this;
    vector<point> ans;
    if ((u.pc | v.pc) <= u.r + v.r + EPS) return ans;
    if (u.r < v.r) swap(u, v);
    ld beta = acos((u.r - v.r) / (u.pc | v.pc));
    point aux = (((v.pc - u.pc).unit()) * u.r);
    ans.push_back(u.pc + (aux % (beta)));
    ans.push_back(u.pc + (aux % (-beta)));
    aux = ((u.pc - v.pc).unit()) * (-v.r);
    ans.push_back(v.pc + (aux % (beta)));
    ans.push_back(v.pc + (aux % (-beta)));
    return ans;
  }
  vector<point> intersection(circle v) {
    vector<point> ans;
    ld d = pc | v.pc;
    if (d > r + v.r + EPS || d + EPS < (max(v.r, r) - min(v.r, r)))
      return ans;
    ld a = (r * r - v.r * v.r + d * d) / (2.0 * d);
    ld c = sqrtl(fabs(r * r - a * a));
    point AB = (v.pc - pc).unit();
    point D = pc + (AB * a);
    point C = D + (AB.ort() * c);
    ans.push_back(C);
    C = D - (AB.ort() * c);
    if (c > EPS) ans.push_back(C);
    return ans;
  }
};

point low;
bool cmpx(point a, point b) { return a < b; }
bool comp(point a, point b) {
  int o = low.ori(a, b);
  if (o == 0) return (low | a) < (low | b);
  return (o == 1) ? 1 : 0;
}

vector<point> ConvexHull(vector<point> A) {
  deque<point> CH;
  int n = A.size();
  low = A[0];
  for (int i = 0; i < n; i++)
    low = min(low, A[i], cmpx);
  sort(A.begin(), A.end(), comp);
  CH.clear();
  CH.push_front(A[0]);
  CH.push_front(A[1]);
  for (int i = 2; i < n; i++) {
    while (CH.size() >= 2 and CH[1].ori(CH[0], A[i]) <= 0) {
      CH.pop_front();
    }
    CH.push_front(A[i]);
  }
  return vector<point>(CH.begin(), CH.end());
}

ld distanciaL(circle A, circle B) {
  if (B.r < A.r)
    swap(A, B);
  ld aux = ld(A.r - B.r) / (A.pc | B.pc);
  ld alpha = acos(aux);
  ld L = sin(alpha) * (A.pc | B.pc);
  return (L);
}

ld distanciaA(circle C, point A, point B) {
  ld d = sqrt(C.r * C.r - ((B - A).mod2() / 4.0));
  ld tita = 2.0 * acos(d / C.r);
  ld ang = C.pc.ang(A, B);
  if (((A - C.pc) ^ (B - C.pc)) < EPS) {
    tita = 2.0 * PI - tita;
  }
  return (tita * C.r);
}

///////////////////////////////////////////////////////////////////

///////////////////////////////////
struct PointPolar {
  double dis, ang;
  PointPolar() {
    dis = 0.0;
    ang = 0.0;
  }
  PointPolar(point a) {
    dis = point()|a;
    double den = (double)a.y;
    double num = hypot(a.x, a.y);
    ang = den / num;
  }
  PointPolar(double _dis, double _ang) {
    dis = _dis;
    ang = _ang;
  }
};
void PrintPPolar(PointPolar a) { cout << "(" << a.dis << "," << a.ang << ") "; }
bool CompDis(PointPolar a, PointPolar b) {
  if ((a.dis != b.dis))
    return a.dis < b.dis;
  return a.ang < b.ang;
}

bool CompAng(PointPolar a, PointPolar b) {
  if (a.ang != b.ang)
    return a.ang < b.ang;
  return a.dis < b.dis;
}

int main(){ return 0; }
