#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

struct P {
	ll x, y, ox, oy;
	int j;
	P(){x=y=0LL;}
	P(ll _x, ll _y) {
		x = _x, y = _y; ox=oy=0LL; j=0;
	}
	P(ll _x, ll _y, ll _ox, ll _oy, int _j) {
		x = _x, y = _y; ox=_ox; oy=_oy; j=_j;
	}
};

ll dist(P a, P b){
	ll u=a.x-b.x;
	ll v=a.y-b.y;
	return u*u+v*v;
}
int coor(P a){
	if(a.x>0LL and a.y>=0LL) return 1;
	if(a.x==0LL and a.y>0LL) return 2;
	if(a.x<0LL) return 3;
	if(a.x==0LL and a.y<0LL) return 4;
	if(a.x>0LL and a.y<0LL) return 5;
	return 0;
	
}
bool CompAng(P a, P b){
	P ao=P(a.x-a.ox, a.y-a.oy);
	P bo=P(b.x-b.ox, b.y-b.oy);
	int ca=coor(ao), cb=coor(bo);
	if(ca!=cb) return ca<cb;	
	if(ao.y*bo.x!=bo.y*ao.x) return ao.y*bo.x<bo.y*ao.x;
	return dist(P(a.ox, a.oy), a)<dist(P(a.ox, a.oy), b);
}
bool igual(P a, P b){
	P ao=P(a.x-a.ox, a.y-a.oy);
	P bo=P(b.x-b.ox, b.y-b.oy);
	int ca=coor(ao), cb=coor(bo);
	if(ca!=cb) return 0;
	if(ao.y*bo.x!=bo.y*ao.x) return 0;
	return 1;
}

P K[10];
vector<P> Vp[10];
int main(){
	int k, n;
	while(scanf("%d %d\n", &k, &n)!=EOF){
		for(int i=0; i<k ;i++){
			cin>>K[i].x>>K[i].y;
			Vp[i].clear();
		}
		ll a, b;
		for(int i=0; i<n; i++){
			cin>>a>>b;
			for(int j=0; j<k; j++){
				Vp[j].push_back(P(a, b, K[j].x, K[j].y, i));
			}
		}
		for(int i=0; i<k; i++){
			sort(Vp[i].begin(), Vp[i].end(), CompAng);
			/*cout<<"I:"<<i<<" "<<K[i].x<<" "<<K[i].y<<" "<<endl;			
			for(int j=0; j<n; j++){
				P aux(Vp[i][j].x-Vp[i][j].ox, Vp[i][j].y-Vp[i][j].oy);
				//cout<<Vp[i][j].ox<<" "<<Vp[i][j].oy<<" "<<Vp[i][j].j<<endl;
				//cout<<Vp[i][j].x<<","<<Vp[i][j].y<<":"<<Vp[i][j].j<<" aux:"<<aux.x<<","<<aux.y<<":"<<coor(aux)<<endl;	
				}*/
		}
		bool A[1100];
		int Per[7]; for(int i=0; i<7; i++) Per[i]=i;
		int Mx=0;
		do{
			//for(int i=0; i<k; i++) cout<<P[i]<<" "; cout<<endl;
			memset(A, 0, n);			
			for(int i=0; i<k; i++){
				int I=Per[i];
				bool sw=1;
				P aux=K[I];
				for(int j=0; j<n; j++){
					if(A[ Vp[I][j].j ]) continue;
					if( !igual(aux, Vp[I][j] ) or sw){
						A[ Vp[I][j].j ]=1; sw=0; aux=Vp[I][j];
						//cout<<"m:"<<Vp[I][j].j<<" ";
					}
				}//cout<<endl;
			}
			int cnt=0;
			for(int i=0; i<n; i++) cnt+=A[i];
			Mx=max(Mx, cnt);
		}while(next_permutation(Per, Per+k));
		printf("%d\n", Mx);
	}
	return 0;
}
