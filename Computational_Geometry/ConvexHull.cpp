//Require point: ori
point low;
bool cmpx(point a, point b) { return a < b; }
bool comp(point a, point b) {
  int o = low.ori(a, b);
  if (o == 0) return (low | a) < (low | b);
  return (o == 1) ? 1 : 0;
}

vector<point> ConvexHull(vector<point> A) {
  deque<point> CH;
  int n = A.size();
  low = A[0];
  for (int i = 0; i < n; i++)
    low = min(low, A[i], cmpx);
  sort(A.begin(), A.end(), comp);
  CH.clear();
  CH.push_front(A[0]);
  CH.push_front(A[1]);
  for (int i = 2; i < n; i++) {
    while (CH.size() >= 2 and CH[1].ori(CH[0], A[i]) <= 0) {
      CH.pop_front();
    }
    CH.push_front(A[i]);
  }
  return vector<point>(CH.begin(), CH.end());
}
