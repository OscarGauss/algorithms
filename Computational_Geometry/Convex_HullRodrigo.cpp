#include <bits/stdc++.h>
using namespace std;
#define x first
#define y second
typedef long long ll;
typedef long double ld;
typedef pair<ll,ll>pt;
typedef vector<pt> vp;
vp A;
deque<pt>CH;
pt low;
ll dist(pt a, pt b){
	ll u=a.x-b.x;
	ll v=a.y-b.y;
	return u*u+v*v;
}
ll area(pt a, pt b, pt c){
	pt u = pt(b.x-a.x,b.y-a.y);
	pt v = pt(c.x-a.x,c.y-a.y);
	ll r = u.x*v.y - v.x*u.y;
	return r;
}
int orientation(pt a, pt b, pt c){
	ll r=area(a,b,c);
	if(r==0)return 0;
	return abs(r)/r;
}
bool comp(pt a, pt b){
	int o=orientation(low,a,b);
	if(o==0) return dist(low,a)<dist(low,b);
	return o==1?1:0;
}
void ConvexHull(){
	int n=A.size();
	low=A[0];
	for(int i=0;i<n;i++)low=min(low,A[i]);
	sort(A.begin(),A.end(),comp);
	CH.clear();
   	CH.push_front(A[0]);
	CH.push_front(A[1]);
	for(int i=2;i<n;i++){
		while(CH.size()>=2 and orientation(CH[1],CH[0],A[i])<=0){
			CH.pop_front();
		}
		CH.push_front(A[i]);
	}
}
int main(){
	int n,m;
	ld p,a,b;
	ll u,v;
	while(cin>>n){
		A.clear();
		ll x,y;
		for(int i=0;i<n;i++){
			cin>>x>>y;
			A.push_back(pt(x-1,y));
			A.push_back(pt(x,y-1));
			A.push_back(pt(x+1,y));
			A.push_back(pt(x,y+1));
		}
		ConvexHull();
		m=CH.size();
		a=b=0.0;
		for(int i=0;i<m;i++){
			u=abs(CH[i].x-CH[(i+1)%m].x);
			v=abs(CH[i].y-CH[(i+1)%m].y);
			a+=min(u,v);
			b+=max(u,v)-min(u,v);
		}
		p=sqrtl(2)*a+b;
		printf("%.7lf\n",(double)p);
	}
	return 0;
}
