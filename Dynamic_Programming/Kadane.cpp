#include "bits/stdc++.h"

using namespace std;

int main(){
	int n;
	scanf("%d",&n);
	vector<int> A(n);
	for(int i=0; i<n; i++){
		scanf("%d",&A[i]);
	}
	int sum=0,ans=0;
	for(int i=0; i<n; i++){
		sum+=A[i];
		ans = max(ans, sum);
		if(sum<0)sum=0;
	}
	printf("Max 1D Range Sum = %d\n", ans);
	return 0;
}