#include "bits/stdc++.h"

using namespace std;

int main(){
	int n, m;
	while(scanf("%d %d", &n,&m)!=EOF){
		vector<vector <int> > A(n, vector<int> (m) );
		for(int i=0; i<n; i++){
			for(int j=0; j<m; j++){
				scanf("%d",&A[i][j]);
				if(i>0) A[i][j] += A[i-1][j];
				if(j>0) A[i][j] += A[i][j-1];
				if(i>0 and j>0) A[i][j] -= A[i-1][j-1];
			}
		}
		int MaxSubRect = (1<<31),SubRect;
		for(int i=0; i<n; i++){
			for(int j=0; j<m; j++){
				for(int k=i; k<n; k++){
					for(int l=j; l<m; l++){
						SubRect=A[k][l];
						if(i>0) SubRect -= A[i-1][l];
						if(j>0) SubRect -= A[k][j-1];
						if(i>0 and j>0) SubRect += A[i-1][j-1];
						MaxSubRect = max(MaxSubRect, SubRect);
					}
				}
			}
		}
		printf("%d\n", MaxSubRect);
	}
	return 0;
}