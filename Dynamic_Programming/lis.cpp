#include "bits/stdc++.h"

using namespace std;

int bs(vector<int> seq,vector<int> m, int L, int num){
	int mx = L+1, mn = 0;
	while( (mx-mn) > 1 ){
		int mid = (mx+mn)/2;
		if(seq[ m[mid] ] <num)
			mn = mid;
		else
			mx = mid;
	}
	return mn;
}

vector<int> lis (vector<int> seq){
	int n = seq.size () , L = 0;
	vector<int> m(n+1),p(n);m[0]=-1;
	for ( int i =0;i <n; i ++){
		int j = bs(seq, m, L, seq[i] ) ;
		p[i] = m[j] ;
		if( j==L or seq [i]<seq[ m[j+1] ]) {
			m[j+1] = i ;
			L = max(L, j+1);
		}
	}
	vector<int> res ;
	int t = m[L] ;
	while( t!=-1 ){
		res.push_back(seq[t] ) ;
		t = p[t] ;
	}
	reverse(res.begin() ,res.end() ) ;
	return res ;
}


int main(){
	int n;
	while(scanf("%d\n",&n)!=EOF){
		vector<int> A(n);
		for(int i=0; i<n; i++){
			scanf("%d",&A[i]);
		}
		vector<int> l = lis(A);
		for(int i=0; i<l.size(); i++){
			printf("%d ", l[i]);
		}printf("\n");
	}
	return 0;
}