#include <iostream>

using namespace std;

int main()
{
     int nc = 5, n=100000;
     int coin[5]= {1,5,10,25,50};
     int v[n+1];
     v[0] = 1;
     for(int i=0;i<nc;i++){
          for(int j=1;j<=n;j++){
               v[j] += v[j-coin[i]];
          }
     }
     int x;
     while(cin>>x){
          cout<<v[x]<<endl;
     }
    //cout << "Hello world!" << endl;
    return 0;
}
