#include "bits/stdc++.h"

using namespace std;

typedef pair<int,int> ii;
typedef pair<int,ii> iii;

iii Kadane(vector<int> A){
	int sum=0,ans=0,ini=0,end=0,ini2=0,end2=0;
	for(int i=0; i<n; i++){
		sum+=A[i];end2=i;
		if(sum>ans or (sum==ans and end2-ini2 > end-ini) ){
			end=end2;
			ini=ini2;
			ans=sum;
		}
		if(sum<0){
			sum=0;
			ini2 = i+1;
			end2 = i;
		}
	}
	return iii(ans,ii(ini+1,end+1) );
}

int main(){
	int n,t;
	while(scanf("%d", &n)!=EOF){
		vector<int> A(n);
		for(int i=0; i<n; i++){
			scanf("%d",&A[i]);
		}
		iii k = Kadane(A);
		if(k.first){
			printf("Max 1D Range Sum = %d is between %d and %d\n", k.first, k.second.first, k.second.first);
		}else{
			printf("Not Max 1D Range Sum\n");
		}
	}
	
	return 0;
}