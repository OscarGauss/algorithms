#include "bits/stdc++.h"

using namespace std;

typedef long long ll;

bool CheckBit(int x, int i){ // i begin to 0
	return (x & ( 1<<i ) )? true: false;
}

int SetBit(int x, int i){
	return (x | (1<<i) );
}

int ResetBit(int x, int i){
	return (x & ~(1<<i) );
}

int ToggleBit(int x, int i){
	return (x ^ (1<<i));
}

int LSOneBit(int x){ //Least Significant One Bit
	return (x & -x);
}

void WalkBits(int x){
	if (x > 1) WalkBits(x>>1);
     printf("%d", x&1 );
}
void PrintBits(int x){
	 WalkBits(x);printf("\n");
}
//////
vector<ll> P;

ll BinaryExponentiation(ll base,ll expo){
	if(!expo) return 1;
	ll temp = BinaryExponentiation(base, expo/2);
	temp*=temp;
	if(expo&1) temp*=base;
	return temp;
}
void fill(){
	P.push_back(1);
	for(int i=1; i<=55; i++){
		P.push_back(BinaryExponentiation(2, i));
	}
}

ll MSOneBit(ll x){ //Most Significant One Bit. First fill() 
	return *((upper_bound(P.begin(), P.end(), x))-1);
}
//////
int main(){
	int ni = sizeof(int)*8; 
	int nlli = sizeof(long long int)*8;
	printf("ni: %d nlli: %d\n" , ni, nlli);
	int n=42;
	PrintBits(n);
	n=SetBit(n, 0);
	PrintBits(n);
	n=ResetBit(n,1);
	PrintBits(n);
	n=ToggleBit(n,2);
	PrintBits(n);
	printf("%d\n", LSOneBit(n));
	//integer : −2,147,483,648 to +2,147,483,647
	int mx=(1<<31);	//−2,147,483,648 
	int mn=(1<<31)-1; //+2,147,483,647
	n=(1<<31)-1; //max int
	printf("%d \n",n);
	PrintBits(n);
	//Contando de Izquierda a Derecha
	PrintBits(n);
	int ua = n & 0x00000000FFFFFFFF; //Los ultimos 32 bits 
	int ub = n & 0x0000000FFFFFFF; //Los ultimos 28 bits
	int uc = n & 0x000000FFFFFF; //Los ultimos 24 bits
	int ud = n & 0x00000FFFFF; //Los ultimos 20 bits
	int ue = n & 0x0000FFFF; //Los ultimos 16 bits
	int uf = n & 0x000FFF; //Los ultimos 12 bits
	int ug = n & 0x00FF; //Los ultimos 8 bits
	int uh = n & 0x0F; //Los ultimos 4 bits
	int pa = n & 0xFFFFFFFF00000000; //Los primeros 32 bits
	int pb = n & 0xFFFFFFF0000000; //Los primeros 28 bits
	int pc = n & 0xFFFFFF000000; //Los primeros 24 bits
	int pd = n & 0xFFFFF00000; //Los primeros 20 bits
	int pe = n & 0xFFFF0000; //Los primeros 16 bits
	int pf = n & 0xFFF000; //Los primeros 12 bits
	int pg = n & 0xFF00; //Los primeros 8 bits
	int ph = n & 0xF0; //Los primeros 4 bits
	printf("%d  ", ua);PrintBits(ua);
	printf("%d  ", ub);PrintBits(ub);
	printf("%d  ", uc);PrintBits(uc);
	printf("%d  ", ud);PrintBits(ud);
	printf("%d  ", ue);PrintBits(ue);
	printf("%d  ", uf);PrintBits(uf);
	printf("%d  ", ug);PrintBits(ug);
	printf("%d  ", uh);PrintBits(uh);
	n=42;
	PrintBits(n);
	ph = n & 0xF0; //Los primeros 4 bits
	PrintBits(ph);
	uh = n & 0x0F; //Los ultimos 4 bits
	PrintBits(uh);
	cout<<(2<<16)<<endl;
	while(cin>>n){
		cout<<LSOneBit(n)<<endl;
	}
	return 0;
}