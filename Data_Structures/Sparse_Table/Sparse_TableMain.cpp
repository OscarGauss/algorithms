#include <bits/stdc++.h>
using namespace std;
#define N 500050
#define LN 20

long long A[N];
int ST[LN][N];

void STInit(int n){
	for(int j=0; j<n; j++) ST[0][j]=j;
	for(int i=1; (1<<i )<=n; i++)
		for(int j=0; j+(1<<i)<=n; j++){
			int a=ST[i-1][j]; // primera mitad
			int b=ST[i-1][j+(1<<(i-1))]; // segunda mitad
			if( A[a]<A[b]) ST[i][j]=a;
			else ST[i][j]=b;			
		}
}

int STQuery(int I, int J){
	int i=32-__builtin_clz(J-I+1); i--;
	if( A[ ST[i][I] ] < A[ ST[i][ J-(1<<i)+1 ] ] )
		return ST[i][I];
	return ST[i][J-(1<<i)+1];
}


int main(){
	int n, m, a, b;
	while(scanf("%d", &n)!=EOF){
		for(int i=0; i<n; i++){
			scanf("%lld", &A[i]);
		}
		STInit(n);
		scanf("%d", &m);
		for(; m--;){
			scanf("%d %d", &a, &b);
			printf("%d\n", A[ STQuery(a, b) ]);
		}
	}
	return 0;
}
