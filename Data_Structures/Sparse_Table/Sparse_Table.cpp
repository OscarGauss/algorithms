void StInit(int n, long long A[], int St[][500050]){
	for(int j=0; j<n; j++) St[0][j]=j;
	for(int i=1; (1<<i )<=n; i++)
		for(int j=0; j+(1<<i)<=n; j++){
			int a=St[i-1][j]; // primera mitad
			int b=St[i-1][j+(1<<(i-1))]; // segunda mitad
			if( A[a]<A[b]) St[i][j]=a;
			else St[i][j]=b;
			
		}
}

int StQuery(int I, int J, long long A[],int St[][500050]){
	int i=32-__builtin_clz(J-I+1); i--;
	if( A[ St[i][I] ] < A[ St[i][ J-(1<<i)+1 ] ] )
		return St[i][I];
	return St[i][J-(1<<i)+1];
}
