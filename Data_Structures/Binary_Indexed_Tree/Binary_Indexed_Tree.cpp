#include <bits/stdc++.h>

using namespace std;

#define LSOneBit(x) ( x & ( -x ) )
#define Type int

Type A[1000001];
Type Tree[1000001]; //el arbol empieza de 1 siempre
Type N;

int MSOneBit(int x){
	int j=0;
	for(int i=0; i<32; i++){
		if(x&(1<<i)){
			j=i;
		}
	}
	return 1<<j;
}

Type Acum(int i){
	int Ans=0;
	for(; i>0; i-=LSOneBit(i))
		Ans+=Tree[i];
	return Ans;
}

Type Acum(int i, int j){
	return Acum(j)-Acum(i-1);
}

Type ReadSingle(int i){
	Type Ans=Tree[i];
	if(i>0){
		int j=i-LSOneBit(i);
		i--;
		for(; i!=j; i-=LSOneBit(i))
			Ans-=Tree[i];
	}
	return Ans;
}

Type UpdateInc(int i, int value){
	for(; i<=N; i+=LSOneBit(i))
		Tree[i]+=value;
}

void Build(){
	memset(Tree, 0, sizeof Tree);
	for(int i=1; i<=N; i++)
		UpdateInc(i, A[i]);
}

void Scale(int c){
	for(int i=1; i<=N; i++)
		Tree[i]/=c;
}

int Find(Type AcumFre){ //Funcionan cuando N es pow 2 revisar
	int i=0, bitMask=MSOneBit(N);	
	while((bitMask) && (i<N)){
		int ti=i+bitMask;
		if(AcumFre==Tree[ti])
			return ti;
		else
			if(AcumFre>Tree[ti]){
				i=ti;
				AcumFre-=Tree[ti];
			}
		bitMask>>=1;
	}
	if(AcumFre)
		return -1;
	return i;
}

int FindG(Type AcumFre){
	int i=0, bitMask=MSOneBit(N);
	while((bitMask) && (i<N)){
		int ti=i+bitMask;
		if(AcumFre>=Tree[ti]){
			i=ti;
			AcumFre-=Tree[ti];
		}
		bitMask>>=1;
	}
	if(AcumFre)
		return -1;
	return i;
}

int main(){
	int i, j, q;
	scanf("%d", &N);
	for(int i=1; i<=N; i++){
		scanf("%d", &A[i]);
	}
	Build();
	scanf("%d", &q);
	while(q--){
		scanf("%d %d", &i, &j);
		printf("La acum de %d a %d es %d\n", i, j, Acum(i, j));
	}
	scanf("%d", &q);
	while(q--){
		scanf("%d", &i);
		printf("Acum %d esta en %d\n", i, Find(i));
	}
	scanf("%d", &q);
	while(q--){
		scanf("%d", &i);
		printf("Acum %d esta en %d\n", i, FindG(i));
	}
	return 0;  
}
