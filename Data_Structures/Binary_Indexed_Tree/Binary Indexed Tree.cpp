typedef long long Tipe;
int MaxVal = (2<<17); //262144 = 2^18 **size of the vector**
vector<Tipe> Tree(MaxVal+1); // [1,n]
vector<Tipe> A(MaxVal+1); // [1,n]

int LSOneBit(int x){ //Least Significant One Bit
	return (x & -x);
}

Tipe Read(int idx){
	Tipe ans = 0;
	while (idx > 0){
		ans += Tree[idx]; idx -= LSOneBit(idx);
	}
	return ans;
}

Tipe Read(int idx_i,int idx_j){
	return idx_i > 1 ? Read(idx_j) - Read(idx_i-1) : Read(idx_j);
}

Tipe ReadSingle(int idx){
	Tipe ans = Tree[idx]; // ans will be decreased
	if (idx > 0){ // special case
		int z = idx - LSOneBit(idx); // make z first
		idx--; // idx is no important any more, so instead y, you can use idx
		while (idx != z){ // at some iteration idx (y) will become z
			ans -= Tree[idx]; idx -= LSOneBit(idx);// substruct Tree frequency which is between y and "the same path"
		}
	}
	return ans;
}

int find(int cumFre){ // This returns any of the indices with the same cumulative frequency.
	int idx = 0, bitMask = MaxVal; // bitMask - initialy, it is the greatest bit of MaxVal, bitMask store interval which should be searched
	while ((bitMask != 0) && (idx < MaxVal)){ // nobody likes overflow :)
		int tIdx = idx + bitMask; // we make midpoint of interval
		if (cumFre == Tree[tIdx]) // if it is equal, we just return idx
			return tIdx;
		else 
			if (cumFre > Tree[tIdx]){ // if Tree frequency "can fit" into cumFre, then include it
				idx = tIdx; // update index 
				cumFre -= Tree[tIdx]; // set frequency for next loop 
			}
		bitMask >>= 1; // half current interval
	}
	if (cumFre == 0) // maybe given cumulative frequency doesn't exist
		return idx;
	else
		return -1;
}

int findG(int cumFre){ // This returns the greatest index with the same cumulative frequency.
	int idx = 0, bitMask = MaxVal; 
	while ((bitMask != 0) && (idx < MaxVal)){
		int tIdx = idx + bitMask;
		if (cumFre >= Tree[tIdx]){ // if current cumulative frequency is equal to cumFre, we are still looking for higher index (if exists)
			idx = tIdx;
			cumFre -= Tree[tIdx];
		}
		bitMask >>= 1;
	}
	if (cumFre == 0)
		return idx;
	else
		return -1;
}

void scale(int factor){ //multiplies all f[i] (and therefore c[i]) by factor
	for(int idx = 1; idx <= MaxVal; ++idx) Tree[idx] *= factor;
}

void Update(int idx ,Tipe val,int n){
	A[idx] += val;
	while (idx <= n){
		Tree[idx] += val; idx += LSOneBit(idx);
	}
}

void BuildTree(int n){
	Tree.assign(n+1, 0); // filled with the neutral element
	for(int i=1; i <=n ; i++){
		int idx=i;
		while (idx <= n){
			Tree[idx] += A[i]; idx += LSOneBit(idx);
		}
	}
}
Index
1 2 4 8
2 4 8 16
3 4 8 16 32
4 4 16 32 64
5 6 8 16 32
6 8 16 32 64
7 8 16 32 64
8 16 32 64
9 10 12 16 32 64
10 12 16 