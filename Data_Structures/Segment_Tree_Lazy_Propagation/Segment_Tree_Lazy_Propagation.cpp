#include <bits/stdc++.h>

using namespace std;

#define Left(n)   ( (n<<1)    )
#define Right(n)  ( (n<<1) +1 )
#define MidL(i,j) ( ( (i+j)>>1 )    )
#define MidR(i,j) ( ( (i+j)>>1 ) +1 )
#define Type int

Type Tree[1048576+2];
Type Lazy[1048576+2];
Type A[500000];

Type Oper(Type A, Type B){ return A+B; }
Type Oper(){ return 0; }
void OperP(Type &Ans, int NumNodes, Type value){ Ans+=NumNodes*value; }
void OperP(Type &Ans, Type value){ Ans+=value; }
Type OperP(){ return -1;}

void BuildTree(int node,int p,int q){
	if(p==q){
		Tree[node] = A[p]; return ;
	}
	BuildTree(Left(node) , p, MidL(p,q));
	BuildTree(Right(node), MidR(p,q), q);
	Tree[node] = Oper(Tree[Left(node)], Tree[Right(node)]);
	Lazy[node] = OperP();
}

void Propagar(int node, int p, int q){
	OperP(Tree[Left(node)], MidL(p, q)-p+1, Lazy[node]);
	OperP(Tree[Right(node)], q-MidR(p, q)+1, Lazy[node]);
	if(Lazy[Left(node)]==OperP()) // si esta vacio left
		Lazy[Left(node)]=Lazy[node];
	else
		OperP(Lazy[Left(node)], Lazy[node]);
	if(Lazy[Right(node)]==OperP())
		Lazy[Right(node)]=Lazy[node];
	else
		OperP(Lazy[Right(node)], Lazy[node]);
	Lazy[node] = OperP();
}

void Update(int node, int p, int q, int i, int j, Type value){
	if(i>q or j<p) return ;
	if(i<=p and q<=j){
		OperP(Tree[node], q-p+1, value);
		if(Lazy[node]==OperP())
			Lazy[node]=value;
		else
			OperP(Lazy[node], value);
		return ;
	}
	if(Lazy[node]!=OperP()) Propagar(node, p, q);
	Update(Left(node) , p, MidL(p,q), i, j, value);
	Update(Right(node), MidR(p,q), q, i, j, value);
	Tree[node] = Oper(Tree[Left(node)], Tree[Right(node)]);
}

Type Query(int node, int p,int q, int l, int r){
	if(l > q or r < p ) return Oper();
	if(p >= l and q <= r) return Tree[node];
	if(Lazy[node]!=OperP()) Propagar( node, p ,q );
	return Oper(Query(Left(node), p, MidL(p,q), l, r), Query(Right(node), MidR(p,q), q, l, r));
}

int main(){
	int n, q, x, y, value;
	char c;
	scanf("%d", &n);
	for(int i=1; i<=n; i++)
		scanf("%d", &A[i]);
	BuildTree(1, 1, n);
	scanf("%d%c", &q, &c);
	while(q--){
		scanf("%c", &c);
		if(c=='Q'){
			scanf("%d %d\n", &x, &y);
			printf("Sum %d to %d is %d\n", x, y, Query(1, 1, n, x, y));
		}else{
			scanf("%d %d %d\n", &x, &y, &value);
			Update(1, 1, n, x, y, value);
		}
	}		
	return 0;
}
