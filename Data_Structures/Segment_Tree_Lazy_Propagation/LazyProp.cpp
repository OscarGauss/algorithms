#include "bits/stdc++.h"

using namespace std;

vector<int> Tree(400000+1);
vector<int> Lazy(400000+1,0);
vector<int> A(100000);

void BuildTree(int node, int p, int q){
	int mid = (p+q)/2, left=(node*2)+1, right=(node*2)+2;
	if(p==q){
		Tree[node] = A[p];
		return;
	}
	BuildTree(left,p,mid); BuildTree(right,mid+1,q);
	Tree[node] = Tree[left] + Tree[right];
	Lazy.assign(400000+1,0);
}

void Propagar(int node, int p, int q){
	int mid = (p+q)/2, left=(node*2)+1, right=(node*2)+2;
	Tree[left] += ( (mid-p+1)*Lazy[node] ); Lazy[left] += Lazy[node];
	Tree[right] += ( (q-mid)*Lazy[node] ); Lazy[right] += Lazy[node];
	Lazy[node] =0;
}
	
void Update(int node, int p, int q, int i, int j, int val){
	int mid = (p+q)/2, left=(node*2)+1, right=(node*2)+2;
	if(i>q or j<p) return ;
	if( i<=p and q<=j ){//ojo
		Tree[node] += ((q-p+1)*val);
		Lazy[node] +=val;
		return;
	}
	Propagar(node, p, q);
	Update(left, p, mid, i, j, val); Update(right, mid+1, q, i, j, val);
	Tree[node] = Tree[left] + Tree[right];
}

int Query(int node, int p,int q, int i,int j){
	int mid = (p+q)/2, left=(node*2)+1, right=(node*2)+2;
	if(i>q or j<p)return 0;
	if(i<=p and q<=j) return Tree[node];
	Propagar( node, p ,q );
	return Query(left, p, mid, i, j)+Query(right, mid+1, q, i, j);
}
int main(){
	int n,m,x,y,val;
	while(cin>>n){
		cout<<n<<endl;
		for(int i=0; i<n; i++){
	 		cin>>A[i];
	 	}
	 	BuildTree(0,0,n-1);
		cin>>m;
		while(m--) {
		    cin>>x>>y>>val;
		    Update(0,0,n-1,x-1,y-1,val);
		}
		cin>>m;
		while(m--) {
		    cin>>x>>y;
		    cout<<Query(0,0,n-1,x-1,y-1)<<endl;
		}
	}
	return 0;
}