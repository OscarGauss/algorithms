#include <bits/stdc++.h>
using namespace std;

struct Node{
	int val, r, l; int sw; // 0 sin uso, 1 con dato, 2 hueco
    Node(){r=l=INT_MAX; sw=0;}
	Node(int v){val=v; r=l=INT_MAX; sw=1;}	
};

Node ColaPriori[100000];
int Nodes=0;
int NodesF=0;
void InitCP(){
	for(int i=0; i<Nodes*2; i++)ColaPriori[i]=Node();
	Nodes=0;
	NodesF=0;
}
int Front(){ if(ColaPriori[1].sw==1) return ColaPriori[1].val; else return INT_MAX;}

void Act(int u){ // u tiene val
	if(!u) return ;
	//cout<<"Act: "<<u<<endl;
	int l=u*2, r=(u*2)+1;
	if(ColaPriori[l].sw==0){ ColaPriori[u].l=INT_MAX;}
	else{
		if(ColaPriori[l].sw==1){
			ColaPriori[u].l=min(ColaPriori[l].l, ColaPriori[l].r);
			if(ColaPriori[u].l!=INT_MAX) ColaPriori[u].l++;
		}else{
			ColaPriori[u].l=1;
		}
	}
	if(ColaPriori[r].sw==0){ ColaPriori[u].r=INT_MAX;}
	else{
		if(ColaPriori[r].sw==1){
			ColaPriori[u].r=min(ColaPriori[r].l, ColaPriori[r].r);
			if(ColaPriori[u].r!=INT_MAX) ColaPriori[u].r++;
		}else{
			ColaPriori[u].r=1;
		}
	}
	Act(u/2);
}
void Flot(int u){
	if(u==1) return;
	int p=u/2;
	if(ColaPriori[u].val<ColaPriori[p].val){
		//cout<<ColaPriori[u].val<<" swap "<<ColaPriori[p].val<<endl;
		swap(ColaPriori[u].val,ColaPriori[p].val);
		Flot(p);
	}
}
void Insert(int dat){
	int u=1;
	if(ColaPriori[u].r==ColaPriori[u].l and ColaPriori[u].r==INT_MAX){
		Nodes++;
		ColaPriori[Nodes].val=dat; ColaPriori[Nodes].sw=1; Flot(Nodes); return;
	}
	while((ColaPriori[u].sw==1)){
		if(ColaPriori[u].r<ColaPriori[u].l){
			u=(u*2)+1;
		}else{
			u=(u*2);
		}
	}
	NodesF--;
	ColaPriori[u].sw=1; ColaPriori[u].val=dat;
	Act(u);
	Flot(u);
}
int Size(){
	return Nodes-NodesF;
}
void Pop(){
	int a, b, l, r;	
	int u=1;	
	while(1){
		l=u*2; r=(u*2)+1;
		a=INT_MAX; b=INT_MAX;		
		if(ColaPriori[l].sw==1) a=ColaPriori[l].val;
		if(ColaPriori[r].sw==1) b=ColaPriori[r].val;
		if(min(a, b)==INT_MAX){
			ColaPriori[u].sw=2;
			ColaPriori[u].r=INT_MAX;
			ColaPriori[u].l=INT_MAX;
			//cout<<"Act:: "<<u/2<<endl;
			NodesF++;
			Act(u/2);
			break;
		}
		if(a<b){
			ColaPriori[u].val=a;
			u=l;
		}else{
			ColaPriori[u].val=b;
			u=r;
		}
	}		
}

int main(){
	int a, n;
	
	while(cin>>n){
		if(!n) break;
		//priority_queue<int, vector<int>, greater<int> > pq;
		InitCP();
		for(int i=0; i<n; i++){
			cin>>a;
			//pq.push(a);
			Insert(a);
		}
		int s=0;
		while(Size()>1){
			//int a=pq.top(); pq.pop();
			//int b=pq.top(); pq.pop();
			int a=Front(); Pop();
			int b=Front(); Pop();
			s+=a+b;
			//pq.push(a+b);
			Insert(a+b);
		}
		cout<<s<<"\n";
		
	}
	return 0;
}
