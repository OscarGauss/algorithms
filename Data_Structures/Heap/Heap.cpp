#include <bits/stdc++.h>

using namespace std;

int arrayHeap[500010];
int nHeap;

void initHeap(){ nHeap=0; }

void insertHeap(int a){	
	arrayHeap[++nHeap]=a;
	int ind=nHeap;
	while(ind>1 and arrayHeap[ind/2]<arrayHeap[ind]){ // flotar
			swap(arrayHeap[ind/2], arrayHeap[ind]);
			ind/=2;
	}
}

bool deleteHeap(){
	if(nHeap==0) return false;
	swap(arrayHeap[1], arrayHeap[nHeap]);
	nHeap--;
	int l, r, mxi, ind=1;
	while(1){ // hundir
		l=ind*2; r=(ind*2)+1; mxi=ind;
		if(l<=nHeap) if(arrayHeap[l]>arrayHeap[ind]) mxi=l;
		if(r<=nHeap) if(arrayHeap[r]>arrayHeap[mxi]) mxi=r;
		if(mxi==ind) break;
		swap(arrayHeap[ind], arrayHeap[mxi]);		
		ind=mxi;		
	}
	return true;
}

int maxHeap(){ return arrayHeap[1]; }

int N, d, v, i, e, cnt, t;
pair<int, int> A[10010];

int main(){	
	scanf("%d\n", &t);
	while(t--){		
		scanf("%d\n", &N);
		for(i=0; i<N; i++){
			scanf("%d %d\n", &A[i].first, &A[i].second);		 
		}		
		scanf("%d %d\n", &e, &v);
		for(i=0; i<N; i++){
			A[i].first=e-A[i].first;
		}
		sort(A, A+N);
		initHeap(); //heap
		cnt=0;
		d=0;
		bool sw=v>=e;
		for(i=0; i<N and !sw; i++){
			if(d+v>=e){ sw=1; break; }
			if(d+v>=A[i].first){				
				v-=A[i].first-d;
				d=A[i].first;
				insertHeap(A[i].second); //heap
			}else{
				int mx=maxHeap();
				if(!deleteHeap()) break;
				v+=mx;
				cnt++;
				i--;
			}			
		}
		while(1){
			if(d+v>=e){ sw=1; break; }
			int mx=maxHeap();
			if(!deleteHeap()) break;
			v+=mx;
			cnt++;
		}		
		printf("%d\n", (sw?cnt:-1));
		
	}
	return 0;
}
