int Numsets;
vector<int>  Parent,Rank,Sizes;

void InitSet(int n){
	Rank.assign(n, 0); Sizes.assign(n, 1); Parent.resize(n);
    for(int i=0; i<n; i++) Parent[i] = i;
    Numsets = n;
}

int FindSet(int i){
    return Parent[i]==i ? i : Parent[i] = FindSet(Parent[i]);
}

bool IsSameSet(int i, int j){
    return FindSet(i) == FindSet(j);
}

void UnionSet(int i, int j){
    if(!IsSameSet(i, j)){
        Numsets--;
        int x = FindSet(i), y = FindSet(j);
        if(Rank[x] > Rank[y]){
            Parent[y] = x; Sizes[x] += Sizes[y]; 
        }else{
            Parent[x] = y; Sizes[y] += Sizes[x];
            if(Rank[x] == Rank[y]) Rank[y]++;
        }
    }
}

int NumDisjoinSets(){
    return Numsets;
}

int SizeOfSet(int i){
    return Sizes[FindSet(i)];
}