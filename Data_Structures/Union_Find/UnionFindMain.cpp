#include "bits/stdc++.h"

using namespace std;

int Numsets;
vector<int>  Parent,Rank,Sizes;

void InitSet(int n){
	Rank.assign(n, 0); Sizes.assign(n, 1); Parent.resize(n);
    for(int i=0; i<n; i++) Parent[i] = i;
    Numsets = n;
}

int FindSet(int i){
    return Parent[i]==i ? i : Parent[i] = FindSet(Parent[i]);
}

bool IsSameSet(int i, int j){
    return FindSet(i) == FindSet(j);
}

void UnionSet(int i, int j){
    if(!IsSameSet(i, j)){
        Numsets--;
        int x = FindSet(i), y = FindSet(j);
        if(Rank[x] > Rank[y]){
            Parent[y] = x; Sizes[x] += Sizes[y]; 
        }else{
            Parent[x] = y; Sizes[y] += Sizes[x];
            if(Rank[x] == Rank[y]) Rank[y]++;
        }
    }
}

int NumDisjoinSets(){
    return Numsets;
}

int SizeOfSet(int i){
    return Sizes[FindSet(i)];
}

int main(){
    int n, m, a, b, q;
    scanf("%d %d",&n, &m);
    InitSet(n);
    while(m--){
        scanf("%d %d",&a, &b);
        UnionSet(a-1,b-1);
    }
    scanf("%d",&q);
    while(q--){
        scanf("%d %d",&a, &b);
        if(IsSameSet(a-1, b-1))
            printf("el nodo %d esta conectado con el nodo %d\n", a, b);
        else
            printf("el nodo %d no esta conectado con el nodo %d\n", a, b);
    }
    return 0;
}
