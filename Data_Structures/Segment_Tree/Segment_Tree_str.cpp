#include "bits/stdc++.h"
using namespace std;

template <typename T, int x, T A[], T f(T a, T b)>
struct SegmentTree{
	T Tr[x*4], v; int n, i, l, r;	
	void Up(int p, int nd, T v){ Tr[nd] = A[p] = v; }///{ Tr[nd] = p; A[p] = v; }
	int m(int a,int b){ return (a+b)>>1; }	
	void build(int a){ n=a; bd(0, n-1, 1); }
	void bd(int p, int q, int nd){
		if(p==q){ Tr[nd] = A[p]; return ; }
		bd(p, m(p,q), nd<<1 );
		bd(m(p,q)+1, q, (nd<<1)|1 );
		Tr[nd] = f( Tr[nd<<1], Tr[(nd<<1)|1] );
	}
	void update(int a, T b){ i=a; v=b; ud(0, n-1, 1); }
	void ud(int p, int q, int nd){
		if( i < p or i > q ) return ;
		if(p==q){ Up(p, nd, v); return ; }
		ud(p, m(p,q), nd<<1 );
		ud(m(p,q)+1, q, (nd<<1)|1 );
		Tr[nd] = f( Tr[nd<<1], Tr[(nd<<1)|1] ); 
	}
	T query(int a, int b){ l=a; r=b; return qr(0, n-1, 1); }
	T qr(int p,int q, int nd){
		if(p >= l and q <= r) return Tr[nd];
		if(r<=m(p,q)) return qr(p, m(p,q), nd<<1);
		if(l>=m(p,q)+1) return qr(m(p,q)+1, q, (nd<<1)|1);
		return f( qr(p, m(p,q), nd<<1), qr(m(p,q)+1, q, (nd<<1)|1) );
	}
};
typedef long long ll;
const int MAXN=100010;
ll A[MAXN];
ll Op(ll a, ll b){ return a+b; }

SegmentTree<ll, MAXN, A, Op> st;

int main(){
	int n, q, a, b;
	char c;
	scanf("%d\n", &n);
	for(int i=0; i<n; i++)
		scanf("%lld", &A[i]);
	st.build(n);
	scanf("%d\n", &q);
	while(q--){
		scanf("%c %d %d\n", &c, &a, &b);
		a--;
		if(c=='U') st.update(a, b);
		else{
			b--;
			printf("%lld\n", st.query(a,b));
		}
		
	}
	return 0;
}
