#include "bits/stdc++.h"

using namespace std;

#define Right(n) (2*n)
#define Left(n) (2*n+1)
vector<int> Tree(1048576+1);
vector<int> A(100000);

int ForceBrute(int i,int j){
	int sum=0;
	for(int l=i; l<=j; l++){
		sum+=A[l];
	}
	return sum;
}

void BuildTree(int p,int q,int ver){
	if(p==q){
		Tree[ver]=A[p];
		return;
	}
	BuildTree(p,(p+q)/2,(2*ver)+1);
	BuildTree((p+q)/2+1,q,(2*ver)+2);
	Tree[ver]=Tree[(2*ver)+1]+Tree[(2*ver)+2];
}

int Query(int l,int r,int ver,int p,int q){
	int mid=(p+q)/2;
	if(l==p and r==q)
		return Tree[ver];
	if(r<=mid){
		return Query(l,r,(2*ver)+1,p,mid);
	}else{
		if(l>mid){
			return Query(l,r,(2*ver)+2,mid+1,q);
		}else{
			return Query(l,mid,(ver*2)+1,p,mid)+Query(mid+1,r,(ver*2)+2,mid+1,q);
		}
	}
}

void Update(int ind,int value,int ver,int p,int q){
	int mid=(p+q)/2;
	if(p==q and p==ind){
		Tree[ver]=value;
		A[ind]=value;
		return;
	}
	if(ind<=mid){
		Update(ind,value,(ver*2)+1,p,mid);
	}else{
		Update(ind,value,(ver*2)+2,mid+1,q);
	}
	Tree[ver]=Tree[(ver*2)+1]+Tree[(ver*2)+2];
}

int main(){
	int n,m,x,y;
	while(cin>>n){
		cout<<n<<endl;
		for(int i=0; i<n; i++){
	 		cin>>A[i];
	 	}
	 	BuildTree(0,n-1,0);
		for(int i=0; i<n*4; i++)cout<<Tree[i]<<" ";cout<<endl;
		for(int i=0;i<n-1;i++){
			for(int j=i+1; j<n; j++){
				cout<<i<<" "<<j<<endl;
				cout<<Query(i,j,0,0,n-1)<<" ? "<<ForceBrute(i,j);
				if(Query(i,j,0,0,n-1)==ForceBrute(i,j) )
					cout<<" = :) "<<endl;
				else
					cout<<" != :( "<<endl;
			}
		}
		cin>>m;
		while(m--) {
		    cin>>x>>y;
		    Update(x,y,0,0,n-1);
		    for(int i=0; i<n*4; i++)cout<<Tree[i]<<" ";cout<<endl;
		}
	}
	return 0;
}