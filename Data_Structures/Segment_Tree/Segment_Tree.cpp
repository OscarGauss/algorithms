#include "bits/stdc++.h"
using namespace std;
typedef int Type;
#define Mid(i,j)  ( (i+j)>>1 )
#define N 500050

Type Tree[N*4];
Type A[N];

Type Oper(Type A, Type B){ return A+B; }

void Build(int p, int q, int node=1){
	if(p==q){ Tree[node] = A[p]; return ; }
	Build(p, Mid(p,q), node<<1 );
	Build(Mid(p,q)+1, q, (node<<1)|1 );
	Tree[node] = Oper( Tree[node<<1], Tree[(node<<1)|1] );
}
int ind;
Type value;
void Update(int p, int q, int node=1){
	if( ind < p or ind > q ) return ;
	if(p==q){ Tree[node] = A[p] = value; return ; }
	Update(p, Mid(p,q), node<<1 );
	Update(Mid(p,q)+1, q, (node<<1)|1 );
	Tree[node] = Oper( Tree[node<<1], Tree[(node<<1)|1] ); 
}
int l, r;
Type Query(int p,int q, int node=1){
	if(p >= l and q <= r) return Tree[node];
	if(r<=Mid(p,q) ) return Query(p, Mid(p,q), node<<1);
	if(l>=Mid(p,q)+1) return Query(Mid(p,q)+1, q, (node<<1)|1);
	return Oper( Query(p, Mid(p,q), node<<1), Query(Mid(p,q)+1, q, (node<<1)|1) );
}

int main(){
	int n, x, y, q;
	char c;
	scanf("%d\n", &n);
	for(int i=0; i<n; i++)
		scanf("%d", &A[i]);
	Build(0, n-1);
	scanf("%d\n", &q);
	while(q--){		
		scanf("%c", &c);
		if(c=='Q'){
			scanf("%d %d\n", &l, &r);
			printf("Sum %d to %d is %d\n", l, r, Query(0, n-1));
		}else{
			scanf("%d %d\n", &ind, &value);
			Update(0, n-1);
	 	}
	}
	return 0;
}
