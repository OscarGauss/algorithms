#include <iostream>
int n;
int A[100000];
using namespace std;
int BS(int a, int b, int x){
    int c=(a+b)/2;
    if(A[c] == x) return c;
    if(a>=b) return -1;
    if(A[c] > x) return BS(a,c-1,x);
    return BS(c+1,b,x);
}

int main()
{

    while( cin>>n){

        for(int i=0;i<n;i++)cin>>A[i];
        int m;
        cin>>m;
        while(m--){
            int x;
            cin>>x;
            cout<<BS(0,n-1,x)<<endl;
        }
    }
    return 0;
}
