#include "bits/stdc++.h"

using namespace std;

int main(){
	printf("**********VECTOR**********\n");
	vector <int> v;
	printf("empty: %d\n", v.empty());             // currently s is empty, true (1)
	v.push_back(1); v.push_back(2); v.push_back(3);
	printf("push_back: 1 2 3\n");
	printf("front: %d\n", v.front()); // 1
	printf("back: %d\n", v.back()); // 3
	
	printf("\n**********STACK**********\n");
	stack<char> s;
	printf("empty: %d\n", s.empty());             // currently s is empty, true (1)
	s.push('a'); s.push('b'); s.push('c');
	printf("push: a b c\n");
	// stack is LIFO, thus the content of s is currently like this:
	// c <- top
	// b
	// a
	printf("top: %c\n", s.top());                                   // output 'c'
	s.pop();                                                  // pop topmost
	printf("pop;\ntop: %c\n", s.top());                                   // output 'b'
	printf("empty: %d\n", s.empty());        // currently s is not empty, false (0)
	
	printf("\n**********QUEUE**********\n");
	queue<char> q;
	printf("empty: %d\n", q.empty());             // currently q is empty, true (1)
	q.push('a'); q.push('b'); q.push('c');
	printf("push: a b c\n");
	// queue is FIFO, thus the content of s is currently like this:
	//		a 	b 	c 
	//	front 		back
	printf("front: %c\n", q.front());                                  // prints 'a'
	printf("back: %c\n", q.back());                                   // prints 'c'
	// output 'a', 'b', then 'c' (until queue is empty), according to the insertion order above
	while (!q.empty()) {
		printf("front: %c\n", q.front());                      // take the front first
		printf("pop;\n");
		q.pop();									// before popping (dequeue-ing) it
	}
	printf("empty: %d\n", q.empty());        // currently q is empty, true (1)

	printf("\n**********DEQUE**********\n");
	deque<char> d;
	printf("empty: %d\n", q.empty());             // currently q is empty, true (1)
	d.push_back('a'); d.push_back('b'); d.push_back('c');
	printf("push_back: a b c\n");
	printf("front: %c - back:%c\n", d.front(), d.back());               // prints 'a - c'
	d.push_front('d');
	printf("push_front: d\n");
	printf("front: %c - back: %c\n", d.front(), d.back());               // prints 'd - c'
	d.pop_back();
	printf("pop_back;\n");
	printf("front: %c - back:%c\n", d.front(), d.back());               // prints 'd - b'
	d.pop_front();
	printf("pop_front;\n");
	printf("front: %c - back: %c\n", d.front(), d.back());               // prints 'a - b'

	printf("\n**********PRIORITY_QUEUE**********\n");
	priority_queue<int> pq;
	pq.push(100); pq.push(10); pq.push(20); pq.push(100);
	pq.push(70); pq.push(2000); pq.push(70);
	// priority queue will arrange items in 'heap' based
	// on the first key in pair, which is money (integer), largest first
	// if first keys tie, use second key, which is name, largest first

	// the internal content of pq heap MAY be something like this:
	// re-read (max) heap concept if you do not understand this diagram
	// the primary keys are money (integer), secondary keys are names (string)!
	//                 (2000)
	//         (100)           (70)   
	//     (100)   (10)     (20)  (70)

	// let's print out the top 3 person with most money
	printf("push: 100 10 20 100 70 2000 70\n");
	while(!pq.empty()){
		int a = pq.top(); 	// O(1) to access the top / max element
		pq.pop(); 			// O(log n) to delete the top and repair the structure
		printf("top: %d\npop;\n", a);
	}//2000 100 100 70 70 20 10

	printf("\n**********PRIORITY_QUEUE greater**********\n");
	priority_queue< int, vector<int>, greater<int> > pqg;
	pqg.push(-9); pqg.push(2); pqg.push(23); pqg.push(10);
	printf("push: -9 2 23 10\n");
	while(!pqg.empty()){
		int a = pqg.top(); pqg.pop();
		printf("top: %d\npop;\n", a);
	}//-9 2 10 23

	printf("\n**********SET MAP**********\n");
	// note: there are many clever usages of this set/map
	// that you can learn by looking at top coder's codes
	// note, we don't have to use .clear() if we have just initialized the set/map
	set<int> used_values; // used_values.clear();
	map<string, int> mapper; // mapper.clear();

	// suppose we enter these 7 name-score pairs below
	/*
	john 78
	billy 69
	andy 80
	steven 77
	felix 82
	grace 75
	martin 81
	*/
	mapper["john"] = 78;   used_values.insert(78);
	mapper["billy"] = 69;  used_values.insert(69);
	mapper["andy"] = 80;   used_values.insert(80);
	mapper["steven"] = 77; used_values.insert(77);
	mapper["felix"] = 82;  used_values.insert(82);
	mapper["grace"] = 75;  used_values.insert(75);
	mapper["martin"] = 81; used_values.insert(81);

	// then the internal content of mapper MAY be something like this:
	// re-read balanced BST concept if you do not understand this diagram
	// the keys are names (string)!
	//                        (grace,75) 
	//           (billy,69)               (martin,81)   
	//     (andy,80)   (felix,82)    (john,78)  (steven,77)
	// iterating through the content of mapper will give a sorted output
	// based on keys (names)
	for (map<string, int>::iterator it = mapper.begin(); it != mapper.end(); it++)
		printf("%s %d\n", ((string)it->first).c_str(), it->second);

	// map can also be used like this
	printf("steven's score is %d, grace's score is %d\n", mapper["steven"], mapper["grace"]);
	printf("==================\n");

	// interesting usage of lower_bound and upper_bound
	// display data between ["f".."m") ('felix' is included, martin' is excluded)
	for (map<string, int>::iterator it = mapper.lower_bound("f"); it != mapper.upper_bound("m"); it++)
		printf("%s %d\n", ((string)it->first).c_str(), it->second);
	
	// the internal content of used_values MAY be something like this
	// the keys are values (integers)!
	//                 (78) 
	//         (75)            (81)   
	//     (69)    (77)    (80)    (82)

	// O(log n) search, found
	printf("%d\n", *used_values.find(77));
	// returns [69, 75] (these two are before 77 in the inorder traversal of this BST)
	for (set<int>::iterator it = used_values.begin(); it != used_values.lower_bound(77); it++)
		printf("%d,", *it);
	printf("\n");
	// returns [77, 78, 80, 81, 82] (these five are equal or after 77 in the inorder traversal of this BST)
	for (set<int>::iterator it = used_values.lower_bound(77); it != used_values.end(); it++)
		printf("%d,", *it);
	printf("\n");
	// O(log n) search, not found
	if (used_values.find(79) == used_values.end())
		printf("79 not found\n");
	return 0;
}