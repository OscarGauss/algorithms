int max_x = (2<<16),max_y = (2<<16); //131072 = 2^17 **size of the vector**
int Tree[max_x+1][max_y+1] // [1,n]
int A[max_x+1][max_y+1]; // [1,n]

int LSOneBit(int x){ //Least Significant One Bit
	return (x & -x);
}

void Update(int x , int y , int val){ //ADDS val to f[x,y]
	int y1;
	while (x <= max_x){
		y1 = y;
		while (y1 <= max_y){
			Tree[x][y1] += val; y1 += LSOneBit(y1); 
		}
		x += LSOneBit(x); 
	}
}

int Read(int x,int y){
	int ans = 0,y1;
	while(x>0){
		y1 = y;
		while(y1 > 0){
			ans += Tree[x][y1]; y1 -= LSOneBit(y1);
		}
		x -= LSOneBit(x);
	}
	return ans;
}

int Read(int x1,int y1,int x2, int y2){
	int ans = Read(x2, y2);
	if(x1>1) ans -= Read(x1-1, y2);
	if(y1>1) ans -= Read(x2, y1-1);
	if(x1>1 and y1>1) ans+= Read(x1-1, y1-1);
	return ans;
}