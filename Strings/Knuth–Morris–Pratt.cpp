#include <bits/stdc++.h>
using namespace std;

void pre(string cad, int P[]){ // P[i]=|borde| de cad[0,i]
	P[0]=0;
	for(int i=1; i<cad.size(); i++){
		P[i]=P[i-1];
		while(P[i]>0 and cad[i]!=cad[P[i]]) P[i]=P[P[i]-1];
		if(cad[i]==cad[P[i]]) P[i]++;
	}
}

vector<int> kmp(string texto, string patron){
	int P[patron.size()];
	pre(patron, P);
	int k=0;
	vector<int> M;
	for(int i=0; i<texto.size(); i++){
		while(k>0 and patron[k]!=texto[i]) k=P[k-1];
		if(patron[k]==texto[i]) k++;
		if(k==patron.size()){
			M.push_back(i-k+1);
			k=P[k-1];
		}
	}
	return M;
}

int main(){	
	char C[2][1000010];
	int test;
	scanf("%d\n", &test);
	for(int t=0; t<test; t++){
		if(t) printf("\n");
		scanf("%s %s\n", C[0], C[1]);
		vector<int> m = kmp(string(C[0]), string (C[1]));
		if(m.size()){
			printf("%d\n", (int)m.size());
			for(int i=0; i<m.size(); i++){
				if(i) printf(" ");
				printf("%d", m[i]+1);
			}printf("\n");
		}else{
			printf("Not Found\n");
		}
		
	}
	return 0;
}
//http://www.spoj.com/problems/NAJPF/
