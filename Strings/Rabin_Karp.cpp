#include <bits/stdc++.h>

using namespace std;
typedef long long ll;
ll n,m;
ll mdans = 1000000007LL;
//ll mod = 748612377754547;
ll mod = 46852243803962273;
//ll mod = 53418737950115581;
map<ll,ll>M;
map<ll,ll>::iterator it;
ll E[100100];
ll md(ll a){
	return (ll)(a%mod+mod)%mod;
}
void Rabin_Karp(string x){
	M.clear();
	ll ht=0LL;
	ll B=33LL;
	ll ans=1LL;
	for(int i=0;i<m;i++){
		E[i]=ans;
		ans*=B;
		ans%=mod;
		ht=md(ht*B+((ll)(x[i]-'a')));
	}
	M[ht]+=(n-m+1LL);
	for(int i=m;i<n;i++){
		ht=md((ht)-md(((ll)(x[i-m]-'a'))*E[m-1]));
		ht=md(ht*B);
		ht=md(ht+((ll)(x[i]-'a')));
		M[ht]+=(n-(ll)i);
	}
}
int main(){
	ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	int t;
	ll ans,a,b;
	string x;
	int c=1;
	cin>>t;
	while(t--){
		cout<<"Case #"<<c++<<": ";
		cin>>x>>m;
		n=x.size();
		Rabin_Karp(x);
		ans=0LL;
		for(it=M.begin();it!=M.end();it++){
		    a=(*it).second;
			b=a-1LL;
			if(a&1LL)b/=2LL;
			else a/=2LL;
			a%=mdans;b%=mdans;
			a*=b;a%=mdans;
			ans+=a;
			ans%=mdans;
		}
		cout<<ans<<"\n";
	}
	return 0;
}
