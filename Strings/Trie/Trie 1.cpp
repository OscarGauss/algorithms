 
#include <bits/stdc++.h>

using namespace std;

struct Trie{
	Trie* hijos[26];
	bool esfin;
	int aux;
	//vector<int> idhijos;
	Trie(){
		esfin = false;
		aux = 0;
		for(int i = 0; i < 26; ++i)
		hijos[i] = NULL;
	}
};

void insertar(Trie* v, string cad, int i){
	v->aux++;
	if(i == cad.length())
	 	v->esfin = true;
	else{
		int k = cad[i] - 'a';
		//v->aux++;
		if(v->hijos[k] == NULL){
			v->hijos[k] = new Trie();
			//(v->idhijos).push_back(k);
		}
	 	insertar(v->hijos[k], cad, i+1);
	}
}

bool Buscar(Trie* v, string cad, int i){
	if(i==cad.length())
		return true;
	else{
		int k = cad[i] - 'a';
		if(v->hijos[k] == NULL){
			return false;
		}
	 	return Buscar(v->hijos[k], cad, i+1);
	}
}

int Count(Trie* v, string cad, int i){
	//cout<<v->aux<<cad[i]<<"  ";
	if(i==cad.length() or v->aux==1)
		return 0;
	else{
		int k = cad[i] - 'a';
		return Count(v->hijos[k], cad, i+1) + 1;
	}

}

int CountIt(Trie* v, string cad, int i){
	//cout<<v->aux<<cad[i]<<"  ";
	Trie* aux = v;
	int c=0;
	while(!(i==cad.length() or aux->aux==1)){
		int k = cad[i] - 'a';
		c++;
		i++;
		aux = aux->hijos[k];
	}
	return c;
}
int main(){
	//ios::sync_with_stdio(false);cin.tie(0);
	int T, n, c, auc;
	vector<string> VC;
	set<string> s;
	string aux;
	Trie* t;
	cin>>T;
	for(int lol=1; lol<=T; lol++){
		cin>>n;
		VC.resize(n);
		for (int i = 0; i < n; ++i){
			cin>>VC[i];
		}
		c=0;
		t = new Trie();
		for(int i=0; i<n; i++){
			aux="";
			auc=0;
			insertar(t, VC[i], 0);
			//auc=Count(t, VC[i], 0);
			auc=CountIt(t, VC[i], 0);
			//cout<<VC[i]<<auc<<endl;
			c+=auc;
		}
		cout<<"Case #"<<lol<<": "<<c+1<<"\n";
	}
	return 0;
}