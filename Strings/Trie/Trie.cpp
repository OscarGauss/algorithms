#include <bits/stdc++.h>

using namespace std;

struct Trie{
	int t;
	map<char, Trie*> M;
	Trie(){
		t=0;
		M.clear();
	}
};

void insertar(Trie* Root, string cad){
	Trie* Node=Root;
	for(int i=0; i<cad.size(); i++){
		if(Node->M[cad[i]]==NULL){
			Node->M[cad[i]]=new Trie();
		}
		Node=Node->M[cad[i]];
	}
	Node->t++;
}

void bfs(Trie* Root){
	queue<Trie*> Q;
	Q.push(Root);
	int niv0=Root->M.size(), niv1=0, aux=0;
	while(!Q.empty()){
		Trie* Node=Q.front(); Q.pop();		
		for(map<char, Trie*>::iterator it=Node->M.begin(); it!=Node->M.end(); it++){
			cout<<it->first<<" ";
			// para impresion
			aux++;
			niv1+=it->second->M.size();
			if(niv0==aux){
				niv0=niv1;
				aux=0;
				niv1=0;
				cout<<"\n";
			}
			/////
			Q.push(it->second);
		}
	}
}

int main(){
	int n;
	string cad;
	while(cin>>n){
		if(n==-1) break;
		Trie* T=new Trie();
		for(int i=0; i<n; ++i){
			cin>>cad;
			insertar(T, cad);
		}
		bfs(T);
	}
	return 0;
}
