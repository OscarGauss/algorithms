#include <bits/stdc++.h>
using namespace std;
typedef vector<int> vi;
typedef pair<int,int> ii;
typedef vector<ii> vii;
typedef vector<vii> vvii;
 
const int MAX = 100001;
const int MAXINT = 100000000;
 
int n;
vvii G(MAX);
vi D(MAX);
 
void Dijkstra(int s)
{
	for(int i=0;i<n;i++)D[i]=MAXINT;
    set<ii> Q;
    D[s] = 0;
    Q.insert(ii(0,s));
 	
    while(!Q.empty())
    {
        ii top = *Q.begin();
        Q.erase(Q.begin());
        int v = top.second;
        int d = top.first;
 
        for (vii::const_iterator it = G[v].begin(); it != G[v].end(); it++)
        {
            int v2 = it->first;
            int cost = it->second;
            if (D[v2] > D[v] + cost)
            {
                if (D[v2] != MAXINT)
                {
                    Q.erase(Q.find(ii(D[v2], v2)));
                }
                D[v2] = D[v] + cost;
                Q.insert(ii(D[v2], v2));
            }
        }
    }
}
int main(){
	int e,v,s,t,c;
	cin>>c;
	for(int k=1;k<=c;k++){
		cin>>n>>;
		vvii a(MAX);
		G=a;
		while(e--){
			int a,p,b;
			cin>>a>>b>>p;
			G[a].push_back(make_pair(b,p));
		}
		Dijkstra(0);
		if(D[n-1]<MAXINT)
		printf("Case #%d: 0 > %d = %d\n",n-1,k, D[t]);
        else cout<<"-1"<<endl;
	}
	return 0;
}