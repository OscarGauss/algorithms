#include <iostream>
#include <cstring>
using namespace std;
int v[100000];
int T[400000];// 2 * pow(2,int(log2(N))+1)
// padre (n-1)/2
int f(int a, int b){
    return a+b;
}
void build(int ver, int p, int q){
    int med=(p+q)/2, left=2*ver+1,rigth=2*ver+2;

    if(p==q){T[ver]=v[p];return;}
    build(left,p,med);
    build(rigth,med+1,q);
    T[ver]=(T[2*ver+1]+T[2*ver+2]);
}
void update(int ver, int p, int q, int valor, int i){
    int med=(p+q)/2, left=2*ver+1,rigth=2*ver+2;
    if(p==q &&  p==i){
        T[ver]=valor;
        v[i]=valor;
        return;
    }
    if(i<=(p+q)/2)update(left,p,med,valor,i);
    else update(rigth, med+1,q, valor, i);
    T[ver]=f(T[2*ver+1],T[2*ver+2]);
}
int query(int ver, int p, int q, int l, int r){
    int med=(p+q)/2, left=2*ver+1,rigth=2*ver+2;
    if(l==p && r==q){return T[ver];}
    else
    {if(r<=med) return query(left, p, med, l, r);
            else
            {
                if(l>med) return query(rigth, med+1, q, l ,r);
                    else return f(query(left, p, med, l, med) , query(rigth, med+1, q, med+1, r));
            }
    }
}
int main()
{
        int n;    
        cin>>n;
        for(int i=0;i<n;i++)cin>>v[i];
        
        build(0,0,n-1);
        int q,a,b;
        string c;
        while(cin>>c){
            cin>>a>>b;
            if(c.compare("sum")==0)
            cout<<query(0,0,n-1,a-1,b-1)<<" "<<endl;
            else update(0,0,n-1,b,a-1);
        }    
    return 0;
}
