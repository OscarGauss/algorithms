#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
typedef vector <ii> vii;

int p[1000000], num_sets;
void initSet(int n) {
  for (int i = 0; i < n ; i++) p[i] = i;
  num_sets = n;
}
int findSet(int i) {
  return p[i] == i?i:p[i] = findSet(p[i]);
}
bool isSameSet(int i, int j) {
  return findSet(i) == findSet(j);
}
void unionSet(int i, int j) {
  if(!isSameSet(i, j)) num_sets--;
  p[findSet(i)] = findSet(j);
}
vector < pair < int , ii > > lista;
int main(){
	int v,e,t;
	cin>>t;
	while(t--){
		cin>>v>>e;
		lista.clear();
		while(e--){
			int a,b,peso;
			cin>>a>>b>>peso;
			lista.push_back(make_pair(peso,make_pair(a-1,b-1)));
		}
		sort(lista.begin(),lista.end());
		
		initSet(v);
		int s=0;
		int t=0;

		for(int i=0;i<lista.size();i++){
			int a,b,peso;
			a=lista[i].second.first;
			b=lista[i].second.second;
			peso=lista[i].first;
			if(!isSameSet(a,b)){
				unionSet(a,b);
				s+=peso;
				t++;
			}
			if(t==v-1)break;
		}
		cout<<s<<endl;
	}
	return 0;
}