#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> ii;
typedef vector <ii> vii;

vector<int> parent,rank;

void Init(int n){
    parent.assign(n, 0); rank.assign(n, 0);
    for(int i=0; i<n; i++) parent[i] = i;
}

int Find(int i){
    return parent[i]==i ? i : parent[i] = Find(parent[i]);
}

bool IsSameSet(int i, int j){
    return Find(i) == Find(j);
}

void Union(int i, int j){
    if(!IsSameSet(i, j)){
        int x = Find(i), y = Find(j);
        if(rank[x] > rank[y]){
            parent[y] = x;
        }else{
            parent[x] = y;
            if(rank[x] == rank[y])rank[y]++;
        }
    }
}

int MST(vector<pair<int,ii> > List,int v){
	sort(List.begin(),List.end());
	initSet(v);
	int mst=0,t=0;
	for(int i=0; i<List.size(); i++){
		int a=List[i].second.first,b=List[i].second.second,w=List[i].first;
		if(!IsSameSet(a,b)){
			Union(a,b);
			mst+=w;t++;
		}
		if(t==v-1)break;
	}
	return mst;
}

int main(){
	int vertices,edges,t,lol,a,b,w;
	vector<pair<int,ii> > List;
	vector<pair<int,ii> > List2;
	while(scanf("%d",&vertices)!=EOF){
		List.clear();List2.clear();
		for(int i=1; i<vertices; i++)
			scanf("%d %d %d",&lol,&lol,&lol);
		scanf("%d",&k);
		for(int i=0; i<k; i++){
			scanf("%d %d %d",&a,&b,&w);
			List2.push_back(w,ii(a,b));
		}
		scanf("%d",&m);
		while(m--){
			scanf("%d %d %d",&a,&b,&w);
			List.push_back(w,ii(a,b));
		}
		printf("%d\n",MST(List,vertices));
		List.insert(List.end(),List2.begin(),List2.end());
		printf("%d\n",MST(List,vertices));
	}
	return 0;
}