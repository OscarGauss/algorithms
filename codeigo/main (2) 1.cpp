#include <iostream>

using namespace std;

long long gcd ( long long a, long long b )
{
    long long c;
    while ( a != 0 ) {
        c = a;
        a = b%a;
        b = c;
    }
    return b;
}

int main()
{
    int t;
    long long a,b,x;
    cin>>t;
    while(t--){
        cin>>a>>b;
        x=gcd(a,b);
        //cout<<x<<endl;
        if((x&(x-1))==0){
            cout<<"Y"<<endl;
        }else{
            cout<<"N"<<endl;
        }
    }
    return 0;
}
