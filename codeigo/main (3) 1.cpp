#include <iostream>
#include <vector>
#include <cstdio>
#include <cstring>

using namespace std;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector< ii > vii;
int v=100,e=100;
int dfs_num[100];
vector < vii > AdjList (1000);
/// dfs(x)=dfs(Ady(x))
void dfs(int x){

    dfs_num[x]=1; ///marco el nodo x como ya visitado

    for(int i =0;i<(int)AdjList[x].size();i++){ /// recorro todos los nodos a los que llega x. desde el primero (0) hasta el ultimo (AdjList[x].size())
        ii p=AdjList[x][i]; /// el i esimo nodo al que llega x
        if(!dfs_num[p.first]){ /// si no ah sido visitado aun...
            dfs(p.first); /// recorro todos los nodos a los que llega p (.first)
        }

    }

}
int main()
{

    while(cin>>v>>e){ /// v: numero de nodos; e: numero de caminos
        memset(dfs_num,0,sizeof(dfs_num)); /// marco como no visitados a todos los nodos
        for(int i=0;i<e;i++){
            int a,b,costo;
            cin>>a>>b>>costo;
            ii p; /// un par que indica que hay un camino de a hacia b y tiene un costo (b,costo)
            p.first=b-1;
            p.second=costo;
            AdjList[a-1].push_back(p); /// lo inserto en el vector de adyacencias de a
        }

        for(int i=0;i<v;i++){
            if(!dfs_num[i]){ /// recorro todos los nodos a los que llega i si es que este no ah sido visitado aun
                dfs(i);
            }
        }
    }

    return 0;
}
