#include <iostream>
#include <vector>
#include <cstdio>
#include <cstring>
#include <queue>
using namespace std;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector< ii > vii;
int n=100,v=100;
int bfs_num[100];
vector < vii > AdjList (1000);
queue <int> q;
/// No es recursivo.. podria ir en el main
void bfs(int x){
    bfs_num[x]=0; /// Se marca al nodo inicial como nivel 0
    q.push(x);/// Se coloca en espera a x
    while(!q.empty()){
        int u=q.front(); /// se saca un elemnto de la cola
        q.pop(); /// se lo elimina de la cola
        for (int i=0; i<(int)AdjList[u].size(); i++) { /// Se visitan todos los adyacentes de u
            ii p=AdjList[u][i];
            if (!bfs_num[p.first]){ /// Si no sevisito
                bfs_num[p.first] = bfs_num[u] + 1; ///Se visit amrcando con el valor del nodo del que vienen + 1
                q.push(p.first); /// Se coloca en espera...
            }
        }

    }
}
int main()
{

    while(cin>>n>>v){
        memset(bfs_num,0,sizeof(bfs_num));
        for(int i=0;i<v;i++){
            int a,b,costo;
            cin>>a>>b>>costo;
            ii p;
            p.first=b-1;
            p.second=costo;
            AdjList[a-1].push_back(p);
        }

        for(int i=0;i<n;i++){
            if(!bfs_num[i]){
                bfs(i);
            }
        }

    }

    return 0;
}
