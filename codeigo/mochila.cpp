#include <bits/stdc++.h>

using namespace std;
int peso[100000];
int ganancia[100000];
int memo[10000][10000];
int solve(int libre,int n){
    if(libre <=0 || n==0) return 0;
    if(memo[libre][n]!=-1)return memo[libre][n];
    int sol=solve(libre,n-1);
    if(peso[n]<=libre){
        sol=max(sol,ganancia[n]+solve(libre-peso[n],n-1));
    }
    return memo[libre][n]=sol;
}
int main()
{
    //cout << "Hello world!" << endl;
    int val,n;
    while(cin>>val>>n){
        memset(memo,-1,sizeof(memo));
        for(int i=1;i<=n;i++){
            cin>>peso[i]>>ganancia[i];
        }
        cout<<solve(val,n)<<endl;
    }
    return 0;
}
