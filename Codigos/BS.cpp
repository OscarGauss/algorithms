#include <bits/stdc++.h>
using namespace std;
int v[1000];
int BS(int a, int b, int x){
	if(a>b) return -1;
	int c=(a+b)/2;
	if(v[c] == x) return c;
	if(a>=b) return -1;
	if(x>v[c]) return BS(c+1,b,x);
	return BS(a,c-1,x);
}	
int main(){
	int n;
	while(cin>>n){
		for(int i=0;i<n;i++)cin>>v[i];
		int m;
		cin>>m;
		while(m--){
			int x;
			cin>>x;
			cout<<BS(0,n-1,x)<<endl;
		}
	}
	return 0;

}