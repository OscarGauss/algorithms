#include <bits/stdc++.h>
using namespace std;
int factorial(int x){
	if(x<=1)return 1;
	return x*factorial(x-1);
}
int main(){
	double n;
	while(cin>>n){
		cout<<factorial(n)<<endl;
	}
	return 0;
}