#include <bits/stdc++.h>

using namespace std;

#define mp make_pair
#define mt make_tuple
#define pb push_back
#define eb emplace_back
#define sz(x) ((int)(x).size())
#define all(c) (c).begin(),(c).end()
#define forn(i, n) for (int i = 0; i < (n); i++)
typedef long long ll;

void mul(int &aux){
	aux*=2;
}

int main(){
	int aux=2;
	cout<<aux<<endl;
	mul(aux);
	cout<<aux<<endl;
	return 0;  
}
