import java.util.*;
import java.util.regex.*;
import java.text.*;
import java.io.*;
import java.math.*;

public class Main {
	static BufferedReader in;
	static PrintWriter out;
	static StringTokenizer tokenizer;
	static String next() throws IOException{
		while(tokenizer==null||!tokenizer.hasMoreTokens())
			tokenizer=new StringTokenizer(in.readLine());
		return tokenizer.nextToken();
	}
	static int nextInt() throws NumberFormatException, IOException{
		return Integer.parseInt(next());
	}
	static long nextLong() throws NumberFormatException, IOException{
		return Long.parseLong(next());
	}
	static String nextLine() throws NumberFormatException, IOException{
		return in.readLine();
	}
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		in=new BufferedReader(new InputStreamReader(System.in));
		out=new PrintWriter(new BufferedWriter(new OutputStreamWriter(System.out)));
		
		in.close();
		out.flush();
	}
}
