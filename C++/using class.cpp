#include <bits/stdc++.h>

using namespace std;

const double EPS=1e-8;

class Point{
private:
	double x, y;
public:
	Point(){
		x=0; y=0;
	}
	double getX(){
		return x;
	}
	double getY(){
		return y;
	}
	void setX(double _x){ x=_x; }
	void setY(double _y){ y=_y; }
};

bool cmpX(Point a, Point b){
	if(fabs(a.getX()-b.getY())>EPS)
		return a.getX()<b.getX();
	return a.getY()+EPS<b.getY();
}





class PointP{
private:
	double x, y;
//	T z;
public:
	PointP(){
		x=0; y=0;
	}
	double getX()const{
		return x;
	}
	double getY() const{
		return y;
	}
	void setX(double _x){ x=_x; }
	void setY(double _y){ y=_y; }
};

bool operator <(const PointP  a, const PointP b){
    if(abs(a.getX() - b.getX()) > EPS) return a.getX() < b.getX();
    return a.getY() + EPS < b.getY();
}

template <int x,long long  y, class T>
class PointPolar{
public:
	double d, ang;
	T aux;
};
int main(){
	Point a;
	cout<<a.getX()<<" "<<a.getY()<<endl;
	Point b, c;
	c.setX(-1); c.setY(9);
	set<Point, bool (*)(Point , Point)> Sx(cmpX);
	cout<<Sx.size()<<endl;
	Sx.insert(a);
	cout<<Sx.size()<<endl;
	Sx.insert(b);
	cout<<Sx.size()<<endl;
	Sx.insert(c);
	cout<<Sx.size()<<endl;
	set<PointP> SPP;
	PointP aa;
	cout<<aa.getX()<<" "<<aa.getY()<<endl;
	PointP bb, cc;
	cc.setX(100); cc.setY(9);
	SPP.insert(aa);
	SPP.insert(bb);
	SPP.insert(cc);
	cout<<"sPP.s: "<<SPP.size()<<endl;
//	SPP.erase(aa);
	cout<<"sPP.s: "<<SPP.size()<<endl;
	for(set<PointP>::iterator it=SPP.begin(); it!=SPP.end(); it++){
//		(*it).setX(9);
		cout<<(*it).getX()<<endl;
		
//		cout<<(*it)->getX()<<" "<<(*it)->getY<<endl;
	}
	return 0;
}
