#include <bits/stdc++.h>

using namespace std;

struct Point{
	int x, y;
	Point(){
		x=0; y=0;
	}
	Point(int _x, int _y){
		x=_x; y=_y;
	}
};
void Print(Point a){
	cout<<"("<<a.x<<","<<a.y<<") ";
}
bool cmpx(Point a, Point b){
	if(a.x != b.x){
		return a.x < b.x;
	}
	return a.y < b.y;
}

bool cmpy(Point a, Point b){
	if(a.y != b.y){
		return a.y < b.y;
	}
	return a.x < b.x;
}

int main(){
	int n, x, y;
	while(cin>>n){
		set<Point, bool (*)(Point , Point)> sx(cmpx); // bool (*)(Point, Point) is for compx work properly
		set<Point, bool (*)(Point , Point)> sy(cmpy); // because cmpx and cmpy are funtions
		vector<Point> v;
		while(n--){
			cin>>x>>y;
			sx.insert(Point(x,y));
			sy.insert(Point(x,y));
			v.push_back(Point(x,y));
		}
		set<Point>::iterator it;
		for(it=sx.begin(); it!=sx.end(); it++){
			Print(*it);
		}cout<<endl;
		for(it=sy.begin(); it!=sy.end(); it++){
			Print(*it);
		}cout<<endl;
		for(int i=0; i<v.size(); i++){
			Print(v[i]);
		}cout<<endl;
		sort(v.begin(), v.end(), cmpx); // in sort only add the function cmpx or cmpy
		for(int i=0; i<v.size(); i++){
			Print(v[i]);
		}cout<<endl;
		sort(v.begin(), v.end(), cmpy);
		for(int i=0; i<v.size(); i++){
			Print(v[i]);
		}cout<<endl;
	}
	return 0;
}