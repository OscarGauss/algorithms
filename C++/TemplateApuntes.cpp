#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <bitset>
#include <vector>
#include <queue>

using namespace std;

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<int,ii> iii;
typedef vector<int> vi;
typedef vector<ii> vii;

#define INF 1000000000  //1 billon, safer than 2B for Floyd Warshall's
#define ALL(x) x.begin(), x.end()   //Exmpl sort(ALL(V));


//ans = a ? b : c;  //if(a) ans = b; else ans = c;
/*    Para la Lecturaaaa
%c <-- char
%d <-- int
%f <-- float
%lld <-- long long
%llf <-- double

double pi = 2 * acos(0.0);
int n;scanf("%d",&n);
printf("%.*lf \n",n,pi);    //thi si the way to manipulate field width n is the decimals of the number
cout<<pi<<endl;             //5 decimals for default with you use c_out*/
/*bitset reset(),set(), [], test();*/
/*Bitmasks
                         5| 4| 3| 2| 1| 0   <- 0-based indexing from right set{1,5}
                        32|16| 8| 4| 2| 1   <- power of 2
S = 34 (base 10)    =    1| 0| 0| 0| 1| 0   (base 2)
                         F| E| D| C| B| A   <-alternative alphabet label {B,F}
check if j-th bit (from right) of S is on
S=42        (dec)      =    101010  (bin)
j=3; 1<<j=8 (dec)      =    001000  (bin)
                            ------  AND
T=8         (dec)      =    001000  (bin)

Tipo      Rango                                        Dígitos          Tamaño
                                                        significativos   en bytes
 ---------------------------------------------------------------------------------
 Real48    2.9 x 10^-39          .. 1.7 x 10^38                11 – 12          6
 Single    1.5 x 10^–45          .. 3.4 x 10^38                 7 –  8          4
 Double    5.0 x 10^–324         .. 1.7 x 10^308               15 – 16          8
 Extended  3.6 x 10^–4951        .. 1.1 x 10^4932              19 – 20         10
 Comp      –2^63+1               .. 2^63 –1                    19 – 20          8
 Currency  –922337203685477.5808 .. 922337203685477.5807       19 – 20          8
*/
///CodeBlocks: for save as (Alt+f a), for save (Crtl+s), for quit (Ctrl+w), for switch tabs Crtl+,

int main(){
    ///Asserciones
    Si la asercion se cumple Run Time Error en algunos judges o WA*/
    int x=100;
    assert(x<0);

    return 0;
}
