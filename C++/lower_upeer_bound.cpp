#include <bits/stdc++.h>

using namespace std;

set<int> v;
int cou(set<int>::iterator it){
	set<int>::iterator i ;
	int c=0;
	for(i=v.begin(); i!=it; i++){
		c++;
	}
	return c;
}
int main(){
	int n, num, t;
	while(cin>>n){
		v.clear();
		for(int i=0; i<n; i++){
			cin>>num;
			v.insert(num);
		}
		set<int>::iterator it;
		cin>>t;
		while(t--){
			cin>>num;
			it = lower_bound(v.begin(), v.end(), num);
			cout<<"lb:"<<cou(it)<<":"<<*it<<endl;
			cin>>num;
			it = upper_bound(v.begin(), v.end(), num);
			cout<<"lu:"<<cou(it)<<":"<<*it<<endl;
		}
	}
	return 0;
}