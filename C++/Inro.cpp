#include <iostream>

using namespace std;

int main()
{
    //Tipos de dato
    int a=89; // Entero 2^31 -1
    float b; // Punto Flontante
    char c; // Caracter
    string cad; // Cadena
    bool d; // Booleano
    long long e; //2^63 -1
    double f; // Doble presicion
    // Variables
    int aa;
    int aaa=0;
    // Impresión Lectura
    cout << aaa <<" Hola "<< cad<<"\n"; // 0 Hola
    cin>> a;
    //Operadores
    //+,-,*,/
    int suma = a +aa;
    //Casteo
    int Aint = (int)'A';
    //Estructuras de control
    //IF-ELSE
    if(a>aaa) // 89 > 0
    {
        //Por verdad
        cout << a << " Es mayor que " << aaa<<"\n";
    }else{
        //Por falso;
        cout << a << " Es menor o igual que  " <<aaa<<"\n";
    }
    //FOR
    for(int i = 0; i<6; i = i+1){
        //Ejecución
        cout << i <<" ";
    }cout<<"\n";
    //WHILE
    int ab = 5;
    while(ab>0){
        //Ejecución
        ab=ab-1;
        cout<<ab<<" ";
    }cout<<"\n";
    //SWITCH
    char car='a';
    switch(car){
     case 'a' :
        cout<<"es una 'a'";
        break;
     case 'b' : cout<<"es una b";
        break;
    }
    //
    return 0;
}
