#include <bits/stdc++.h>

using namespace std;

#define ll long long
#define ull unsigned long long
#define ld long double
#define mp make_pair
#define pb push_back
#define INF 1000000000
#define ff first
#define ss second
#define ii pair<int,int>
#define LL pair<ll,ll>
#define vi vector<int>
#define vvi vector<vi>
#define vvii vector<vii>
#define vll vector<ll>
#define vii vector<ii>
#define _(A,v) memset(A,v,sizeof(A))
#define sz(c) (int)(c).size()
#define ALL(A) (A).begin(),(A).end()
#define RALL(A) (A).rbegin(),(A).rend()
#define EPS 1e-9
#define MOD INF+7
#define PI acos(-1.0)
#define FOR(i,a,b)for(int i=int(a);i<int(b);i++)
#define ROF(i,a,b)for(int i=int(a)-1;i>=int(b);i--)
#define REP(i,a,b)for(int i=int(a);i<=int(b);i++)
#define PER(i,a,b)for(int i=int(a);i>=int(b);i--)
#define DBG(a) cout<<__LINE__<<": "<<#a<<"= "<<a<<ENDL;
#define NDEBUG
#define FOREACH(it,A) for( __typeof((A).begin())it=(A).begin();it!=(A).end();it++)
#define FASTIO ios::sync_with_stdio(false);cin.tie(0);
#define ENDL '\n'

int main(){
	//freopen("in_.txt","r",stdin);
	//freopen("out_.txt","w",stdout);
    //FASTIO
    
    return 0;
}
