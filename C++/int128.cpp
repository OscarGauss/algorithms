#include <bits/stdc++.h>
using namespace std;

void print_uint128(__int128_t n){
    if (n == 0) { return;}
    print_uint128(n/10);
		cout<<abs((int)(n%10));
}
void print128(__int128_t x){
	if(x<0) cout<<"-";
	print_uint128(x); cout<<endl;
}
int main(){
	__int128_t a=0, uno=1;
	//memset(a, -1, sizeof a); // no se puede
	//cout<< (sizeof a)<<endl; // no se puede
	for( __int128_t i=0; i<128-1; i++){
		a = a | (uno<<i);
	}
	print128(a);//inv(a);
	a=(uno<<127); // ojo 1<<i no valido
	print128(a);
	a=a+1;	
	a*=-1;
	print128(a);
	return 0;
}
 
