							TIPOS DE DATOS EN C++

								DÍGITOS
	TIPOS			TAM. BITS	DE
								PRECISIÓN	RANGO MIN			RANGO MAX
bool					8		0		0							1
char					8		2		-128						127
signed char				8		2		-128						127
unsigned char			8		2		0							255
short int				16		4		-32,768						32,767
unsigned short int		16		4		0							65,535
int						32		9		-2,147,483,648				2,147,483,647
unsigned int			32		9		0							4,294,967,295
long int				32		9		-2,147,483,648				2,147,483,647
unsigned long int		32		9		0							4,294,967,295
long long int			64		18		-9,223,372,036,854,775,808	9,223,372,036,854,775,807
unsigned long long int	64		18		0							18,446,744,073,709,551,615
float					32		6		1.17549e-38					3.40282e+38
double					64		15		2.22507e-308				1.79769e+308
long double				96		18		3.3621e-4932				1.18973e+4932 


#define CHAR_BIT 8             /* number of bits in a char */
#define SCHAR_MIN (-128)       /* minimum signed char value */
#define SCHAR_MAX 127          /* maximum signed char value */
#define UCHAR_MAX 0xff         /* maximum unsigned char value */
