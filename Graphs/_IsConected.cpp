#include "bits/stdc++.h"

using namespace std;

bool IsConnected(vector<vector<int> > AdjList){
    if (AdjList.size() <= 1)
        return true;
 
    vector<bool> visit(AdjList.size(),false);
 	
    set<int> forvisit;
    set<int>::iterator current;
 
    forvisit.insert(0);
    while (!forvisit.empty()){
        current = forvisit.begin();	
        if (!visit[*current]){
            for (int i = 0; i < AdjList[*current].size(); i++){
                if (!visit[ AdjList[*current][i] ])
                    forvisit.insert( AdjList[*current][i] );
            }
        }
        visit[*current] = true;
        forvisit.erase(current);
    }
    bool result=true;
    for (int i=0; i<visit.size(); i++)
        result = result && visit[i];
    return result;
}

int main(){
	int n, m, a, b;
	while(scanf("%d %d", &n, &m)!=EOF){
		if(n==0 and m==0) break;
		vector<vector<int> > AdjList(n);
		for(int i=0; i<m; i++){
			scanf("%d %d", &a, &b);a--;b--;
			AdjList[a].push_back(b);
		}
		if(IsConnected(AdjList)){
			printf("SI\n");
		}else{
			printf("NO\n");
		}
	}
	return 0;
}