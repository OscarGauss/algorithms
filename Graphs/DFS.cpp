#include <bits/stdc++.h>

using namespace std;

vector<int> AdjList[1000];
int Visited[1000];
int Parent[1000];
int Num[10000];


void dfs(int start, int n){
	memset(Visited, 0, n*4);
	memset(Parent, -1, n*4);
	memset(Num, -1, n*4);
	int u=0, idx=0, v;
	Num[u]=idx++;
	do{
		if(Visited[u]==AdjList[u].size()) u=Parent[u];
		else{				
			v=AdjList[u][Visited[u]];
			Visited[u]++;
			if(Num[v]==-1){
				Num[v]=idx++;
				Parent[v]=u;
				u=v;
			}				
		}
	}while(Num[u]!=0);
}


int main(){
	int n, m, u, v, idx, q;
	while(scanf("%d", &n)!=EOF){
		if(!n) break;
		while(scanf("%d", &u)){
			if(!u) break;
			while(scanf("%d", &v)){
				if(!v) break;
				AdjList[u-1].push_back(v-1);
			}
		}
		/*
		for(; m--;){
			scanf("%d %d", &u, &v);
			AdjList[u].push_back(v);
		}
		*/
		for(int i=0; i<n; i++){
			cout<<"i:"<<i<<" ";
			for(int j=0; j<AdjList[i].size(); j++){
				cout<<AdjList[i][j]<<" ";
			}cout<<endl;
		}
		for(int i=0; i<n; i++) cout<<Num[i]<<" "; cout<<endl;
		scanf("%d", &q);
		cout<<q<<endl;
		while(q--){
			scanf("%d", &u);u--;
			dfs(u, n);
			cout<<" U: "<<u<<endl;
			int cnt=0;
			for(int i=0; i<n; i++){
				if(Num[i]==-1) cnt++;
			}
			if(cnt==0) printf("0\n");
			else printf("%d ", cnt);
			for(int i=0; i<n; i++){
				if(Num[i]==-1) printf(" %d", i+1);
			}
		}
	}
	return 0;
}
