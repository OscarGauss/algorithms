 
#include <bits/stdc++.h>

using namespace std;

int a,b,n, m;
vector<vector<int> >G;
vector<int> Dis;
vector<int> IDC;
int cntaux=0;
int contar(int u,int padre){
    int ans=1, aux;
    for(int i=0; i<G[u].size(); i++){
        int v=G[u][i];
        if(v!=padre){
            aux=contar(v, u);
            ans+=aux;
        }
    }
    return Dis[u]=ans;
}

void dfsc(int u, int nod,int id){
    int v=-1;
    v=u;
    while(1){
        v=-1;
        for(int i=0; i<G[u].size(); i++){
            if(Dis[ G[u][i] ] > nod/2 && IDC[ G[u][i] ]==-1){
                v=G[u][i];
                break;
            }
        }
        if(v==-1)break;
        Dis[u]=Dis[u]-Dis[v];
        Dis[v]=nod;
        u=v;
    }
    IDC[u]=id; //centro
    for(int i=0; i<(int)G[u].size(); i++){
        if(IDC[ G[u][i] ]==-1){
            dfsc(G[u][i], Dis[G[u][i]], id+1);

        }
    }
}


int main(){
    while(cin>>n){
        G.assign(n, vector<int> ());
        m=n-1;
        while(m--){
            cin>>a>>b;a--; b--;
            G[a].push_back(b);
            G[b].push_back(a);
        }
        IDC.assign(n, -1);
        Dis.assign(n, -1);
        Dis[0]=contar(0, 0);
        dfsc(0, n, 0);
        /*Dis[0]=contar(0, 0);
        for(int i=0; i<n; i++){
            cout<<Dis[i]<<" ";
        }cout<<endl;*/
        int mx=-1;
        for(int i=0; i<n; i++){
            mx=max(mx, IDC[i]);
        }
        /*
        for(int i=0; i<n; i++){
                cout<<IDC[i]<<" ";
            }cout<<"\n";
        */
        if(mx>'z'-'a'){
            cout<<"Impossible!\n";
        }else{

            for(int i=0; i<n; i++){
                cout<<(char)(IDC[i]+'A')<<" ";
            }cout<<"\n";

        }
        
    }
    return 0;
}