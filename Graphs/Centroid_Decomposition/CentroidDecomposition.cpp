// Codeforces Round #190 (Div. 1) C
#include <bits/stdc++.h>
using namespace std;

const int MAXN=100010;
vector<int> G[MAXN];
int Rnk[MAXN], Parent[MAXN], SubS[MAXN];

void cnt(int u, int p){
	SubS[u]=1;
	for(int i=0; i<(int)G[u].size(); i++){
		int v=G[u][i];
		if(v!=p and Rnk[v]==-1){
			cnt(v, u);
			SubS[u]+=SubS[v];
		}
	}
}

void dfsc(int u, int n, int lvl, int p, int _p){
	for(int i=0; i<(int)G[u].size(); i++){
		int v=G[u][i];
		if(v!=p and SubS[v]> n/2 and Rnk[v]==-1){
			return dfsc(v, n, lvl, u, _p);
		}
	}
	Rnk[u]=lvl;
	Parent[u]=_p; // Aca se guarda el arbol centroid
	cnt(u, u);
	for(int i=0; i<(int)G[u].size(); i++){
		int v=G[u][i];
		if(Rnk[v]==-1){
			dfsc(v, SubS[v], lvl+1, v, u);
		}
	}
}


int main(){
	int n, a, b;
	while(scanf("%d", &n)!=EOF){
		for(int i=0; i<n; i++){
			G[i].clear();
			Rnk[i]=-1;
		}
		for(int i=1; i<n; i++){
			scanf("%d %d", &a, &b); a--; b--;
			G[a].push_back(b);
			G[b].push_back(a);
		}
		cnt(0, 0);
		dfsc(0, n, 0, 0, 0);
		for(int i=0; i<n; i++){
			if(i) printf(" ");
			printf("%c", Rnk[i]+'A');
		}printf("\n");
	}
	return 0;
}
