// QTREE SPOJ 
#include <bits/stdc++.h>

using namespace std;
#define DBG(X) cout<<#X<<"="<<X<<endl;
#define Type ll
typedef long long ll;
typedef pair<int, ll> ii;
const int MAXN=100010;
struct nod{
	int size, cadena, indbas, depth, cabeza; // int indcad;
	int parent[20];
	Type w[2]; // 1 nodo 0- arco peso
	vector<ii> L; // <nodo, peso>
	nod(){
		size=cadena=indbas=depth=-1;
		L.clear();
		w[0]=w[1]=0LL; // representa el peso del arco hacia mi padre o el valor del nodo
		for(int j=0; j<20; j++) parent[j]=-1;
	}
};
nod T[MAXN];

void dfs1(int u, bool &sw){
	T[u].size=1;
	for(int i=0; i<T[u].L.size(); i++){
		int v=T[u].L[i].first;
		if(T[v].size!=-1)continue;
		if(!sw)T[v].w[0]=T[u].L[i].second; // asando el peso del arco al nodo
		T[v].depth=T[u].depth+1;
		T[v].parent[0]=u;
		dfs1(v, sw);
		T[u].size+=T[v].size;
	}
}

int lca(int u, int v){
	if(T[u].depth<T[v].depth) swap(u, v);
	int diff=T[u].depth-T[v].depth;
	for(int i=0; i<20; i++) if((diff>>i)&1) u=T[u].parent[i];
	if(u==v) return u;
	for(int i=19; i>=0; i--)
		if(T[u].parent[i]!=T[v].parent[i]){
			u=T[u].parent[i];
			v=T[v].parent[i];
		}
	return T[u].parent[0];
}

#define Mid(i,j)  ( (i+j)>>1 )
int AST[MAXN]; // guarda los indices del nodo//Cambia el ST
Type ST[MAXN*4];
Type operSt(Type A, Type B){ return max(A, B); }

void buildSt(int p, int q, bool &sw, int node=1){
	if(p==q){ ST[node] = T[AST[p]].w[sw]; return ; }
	buildSt(p, Mid(p,q), sw, node<<1 );
	buildSt(Mid(p,q)+1, q, sw, (node<<1)|1 );
	ST[node] = operSt( ST[node<<1], ST[(node<<1)|1] );
}
void updateSt(int ind, Type value, int p, int q, bool &sw, int node=1){
	if( ind < p or ind > q ) return ;
	if(p==q){ ST[node] = T[AST[p]].w[sw] = value; return ; }
	updateSt(ind, value, p, Mid(p,q), sw, node<<1 );
	updateSt(ind, value, Mid(p,q)+1, q, sw, (node<<1)|1 );
	ST[node] = operSt( ST[node<<1], ST[(node<<1)|1] ); 
}
Type querySt(int l, int r, int p, int q, int node=1){
	if(p >= l and q <= r) return ST[node];
	if(r<=Mid(p,q) ) return querySt(l, r, p, Mid(p,q), node<<1);
	if(l>=Mid(p,q)+1) return querySt(l, r, Mid(p,q)+1, q, (node<<1)|1);
	return operSt( querySt(l, r, p, Mid(p,q), node<<1), querySt(l, r, Mid(p,q)+1, q, (node<<1)|1) );
} 

int Ind; // Ind tiene que ser global
void dfsHld(int u, int Cad=0){
	T[u].cadena=Cad; //T[u].indcad=CadSz[Cad]; CadSz[Cad]++;
	T[u].indbas=Ind++;
	AST[T[u].indbas]=u;
	int Mx=-1, ind=-1;
	for(int i=0; i<T[u].L.size(); i++){
		int v=T[u].L[i].first;
		if(T[v].cadena==-1 and T[v].size>Mx){
			ind=i;
			Mx=T[v].size;
		}		
	}
	if(ind!=-1){
		T[T[u].L[ind].first].cabeza=T[u].cabeza;
		dfsHld(T[u].L[ind].first, Cad);
	}
	for(int i=0; i<T[u].L.size(); i++){
		int v=T[u].L[i].first;
		if(T[v].cadena!=-1) continue;
		T[v].cabeza=v;
		dfsHld(v, Cad+1);
	}
}

Type queryHldUp(int u, int v, int N){ //maneja como si fuera arcos 
	Type ans, ans2;
	bool fg=1;
	while(T[u].cadena!=T[v].cadena){		
		ans2=querySt(T[T[u].cabeza].indbas, T[u].indbas, 0, N-1);
		if(fg){ ans=ans2; fg=0; }
		else ans=operSt(ans, ans2);		
		u=T[T[u].cabeza ].parent[0];
	}
	if(u==v) return ans;
	ans2=querySt(T[v].indbas+1, T[u].indbas, 0, N-1);
	if(fg) return ans2;
	return operSt(ans, ans2);
}
void initHld(int N, bool sw, int root=0){
	T[0].depth=T[0].parent[0]=0;
	dfs1(root, sw);
	for(int i=1; i<20; i++) // Build LCA
		for(int j=0; j<N; j++)
				T[j].parent[i]=T[T[j].parent[i-1] ].parent[i-1];
	Ind=0;
	T[0].cabeza=0;
	dfsHld(root);
	buildSt(0, N-1, sw);
}

void updateHld(int u, Type value, int N, bool sw){
	if(sw)
		updateSt(T[u].indbas, value, 0, N-1, sw);
	else{ //deacuerdo al problema
		T[u].w[0]=value; // el arco que le llega a u del padre
		updateSt(T[u].indbas, value, 0, N-1, sw);
	}
}
Type queryHld(int u, int v, int N, bool sw){ // 1 nodo, 0 arco
	if(u==v) return T[u].w[1]; // solo puede ser si se manejan nodos
	int l=lca(u, v);
	if(u==l) swap(u, v);
	Type ans=queryHldUp(u, l, N);
	if(sw) ans=operSt(T[l].w[1], ans); // agregar el 'arco' del lca
	if(v==l) return ans;
	ans=operSt(queryHldUp(v, l, N), ans);
	return ans;
}
typedef pair<int, int> pr;
vector<pr> E;
int main(){
	int Test, N, a, b;
	Type w;
	char cad[5];
	scanf("%d\n", &Test);
	for(int test=0; test<Test; test++){		
		scanf("%d\n", &N);
		for(int i=0; i<N; i++){ // T es un vector de nodos
			T[i]=*new nod();
		}
		E.clear();
		for(int i=0; i<N-1; i++){
			scanf("%d %d %lld\n", &a, &b, &w); a--; b--;
			T[a].L.push_back(ii(b, w));
			T[b].L.push_back(ii(a, w));
			E.push_back(pr(a,b));
		}
		initHld(N, 0); // 1 nodo, 0 arco
		while(scanf("%s", cad)!=EOF){
			if(!strcmp(cad, "DONE")) break;
			scanf("%d %d\n", &a, &b);
			if(!strcmp(cad, "QUERY")){
				a--; b--;
				printf("%lld\n", queryHld(a, b, N, 0)); // 1 nodo, 0 arco
			}
			if(!strcmp(cad, "CHANGE")){
				a--;
				if(T[E[a].first].parent[0]==E[a].second){
					updateHld(E[a].first, b, N, 0);
				}else{
					updateHld(E[a].second, b, N, 0);
				}					
			}				
		}
	}
	return 0;
}
