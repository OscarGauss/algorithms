 #include "bits/stdc++.h"

using namespace std;

typedef pair<int,int> ii;
typedef vector<ii> vii;

vector<vii> AdjList;
vector<int> Dist;
vector<int> Parent;
bool Bellman_Ford(int vertices, int start){
	Dist.assign(vertices,((1<<31)-1));
	Parent.assign(vertices,-1));
	Dist[start] = 0;
	for(int i=0; i<vertices-1; i++)
		for(int u=0; u<vertices; u++)
			for(int j=0; j<(int)AdjList[u].size(); j++){
				int v = AdjList[u][j].first, w = AdjList[u][j].second;
				if(Dist[v] > Dist[u]+w){ //relax
					Dist[v] = Dist[u]+w;
					Parent[v] = u;
				}
			}
	for(int u=0; u<vertices; u++)
		for(int j=0; j<(int)AdjList[u].size(); j++){
			ii v = AdjList[u][j];
			if(Dist[v.first] > Dist[u]+v.second)
				return false;
		}
	return true;
}

int main(){
	int t, vertices, edges, start, u, v, w;
	scanf("%d", &t);
	while(t--){
		scanf("%d %d\n", &vertices, &edges);
		AdjList.assign(vertices, vector<ii>() );
		for(int i=0; i<edges; i++){
			scanf("%d %d %d", &u, &v, &w);
			AdjList[u].push_back( ii(v, w) );
		}
		scanf("%d", &start);
		if(Bellman_Ford(vertices, start) ){
			printf("No existen ciclos negativos :) Dist[ ");
			for(int i=0; i<vertices; i++)printf("%d, ", Dist[i]);printf(" ]\n");
		}else{
			printf("Existen ciclos negativos :( las distancias son -INF\n");
		}
	}
	return 0;
}