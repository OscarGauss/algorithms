#include <bits/stdc++.h>

using namespace std;

struct Edge{
	int to, rev, cap, flow;
	Edge(int t, int r, int c, int f=0){ to=t; rev=r; cap=c; flow=f;}
};

void AddEdge(vector<Edge> Graph[], int from, int to, int cap, int rcap=0LL){
	Graph[from].push_back(Edge(to, Graph[to].size()    , cap));
	Graph[to].push_back(Edge(from, Graph[from].size()-1, rcap));
}

bool bfs(int source, int sink, int nodes, int Layer[], vector<Edge> Graph[]){
	memset(Layer, -1, nodes*4); Layer[source]=0;
	queue<int> q; q.push(source);
	while(!q.empty()){
		int u=q.front(); q.pop();
		for(int j=0; j<Graph[u].size(); j++){
			Edge &e=Graph[u][j];
			if(e.flow<e.cap and Layer[e.to]==-1){ // edge is useful
				q.push(e.to);
				Layer[e.to]=Layer[u]+1;
			}
		}
	}
	return Layer[sink]!=-1;
}

vector<Edge> G[5050];
int L[5050], blocked[5050];
int f;
int sink;

void dfs(int u, int minEdge=INT_MAX){
	if(u==sink){
		f=minEdge; return ;
	}
	for(int &i=blocked[u]; i<G[u].size(); i++){
		Edge &e=G[u][i];
		if(e.flow<e.cap and L[e.to]==L[u]+1){
			dfs(e.to, min(minEdge, e.cap-e.flow));
			if(f!=INT_MAX){
				e.flow+=f;
				G[e.to][e.rev].flow-=f;
				return ;
			}
		}
	}	
}


long long MaxFlowDinic(int source, int _sink, int nodes){
	long long mf=0;
	sink=_sink;
	while(bfs(source, sink, nodes, L, G)){
		memset(blocked, 0, nodes*4);
		while(1){
			f=INT_MAX;			
			dfs(source);
			if(f==INT_MAX) break;
			mf+=(long long)f;
		}			
	}
	return mf;
}

int main(){
	int a, b, n, m;
	long long w;
	while(scanf("%d %d\n", &n, &m)!=EOF){
		for(int i=0; i<n; i++) G[i].clear();
		while(m--){
			scanf("%d %d %lld\n", &a, &b, &w); a--; b--;
			AddEdge(G, a, b, w, w); // no dirigido
			// AddEdge(G, a, b, w); AddEdge(G, b, a, w); // no dirigido
		}
		long long R=MaxFlowDinic(0, n-1, n);
		printf("%lld\n", R);
	}
	return 0;	
}
