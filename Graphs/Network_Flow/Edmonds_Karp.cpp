#include <bits/stdc++.h>

using namespace std;

vector<int> AdjList[5050];
long long MRes[5050][5050];
bitset<5050> Vis;
int P[5050];

long long MaxFlowEK(int s, int t, int n){
	long long mf=0, minedge;
	while(1){
		Vis.reset(); Vis[s]=1;
		queue<int> q; q.push(s);
		memset(P, -1, n*4);
		while(!q.empty()){
			int u=q.front(); q.pop();
			if(u==t) break;
			for(int j=0; j<AdjList[u].size(); j++){
				int v=AdjList[u][j];
				if(MRes[u][v]>0 and !Vis[v])
					Vis[v]=1, q.push(v), P[v]=u;
			}
		}
		if(P[t]==-1) break;
		minedge=LONG_LONG_MAX;
		for(int u=t; P[u]!=-1; u=P[u]){
			minedge=min(MRes[P[u]][u], minedge);
		}
		for(int u=t; P[u]!=-1; u=P[u]){
			MRes[P[u]][u]-=minedge;
			MRes[u][P[u]]+=minedge;
		}
		mf+=minedge;
	}
	return mf;
}

int main(){
	int a, b, n, m;
	long long w;
	while(scanf("%d %d\n", &n, &m)!=EOF){
		memset(MRes, 0, sizeof MRes);
		for(int i=0; i<n; i++) AdjList[i].clear();
		while(m--){
			scanf("%d %d %lld\n", &a, &b, &w); a--; b--;
			AdjList[a].push_back(b);
			AdjList[b].push_back(a);
			MRes[a][b]+=w;
			MRes[b][a]+=w;
		}
		long long R=MaxFlowEK(0, n-1, n);
		printf("%lld\n", R);
	}
	return 0;	
}
