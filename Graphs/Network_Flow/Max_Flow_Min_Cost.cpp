#include <iostream>
#include <queue>
#include <climits>
#include <vector>
#include <cstring>
#include <map>
using namespace std;

#define MAXN 100010 // cambiar OJO este debe ser de nodos
#define MAXM MAXN * 5 //cambiar OJO y este de arcos 
typedef vector<int> VI;
#define ll long long
#define ull unsigned long long
#define PII pair<int,int>
#define PDD pair<double,double>
#define REP(i,j,k) for(int (i)=(j);(i)<(k);++(i))
const int INF = INT_MAX;
int S; //inicio source
int T; //fin sink
int FN; //cantidad de nodos
int FM; //iniciar siempre con cero
VI ra[MAXN];
int kend[MAXM], cap[MAXM], cost[MAXM]; 
int addedge(int a, int b, int c, int d) {
	int i = 2*FM;
	kend[i] = b;
	cap[i] = c;
	cost[i] = d;
	ra[a].push_back(i);
	kend[i+1] = a;
	cap[i+1] = 0;
	cost[i+1] = -d;
	ra[b].push_back(i+1);
	FM++;
	return i;
}
int n;
int dst[MAXM], pre[MAXM], pret[MAXM];
bool spfa(){
	REP(i,0,FN) dst[i] = INF;
	dst[S] = 0;
	queue<int> que; que.push(S);
	while(!que.empty()){
		int x = que.front(); que.pop();
		for (int i=0;i<ra[x].size();i++){
			int t=ra[x][i];
			int y = kend[t], nw = dst[x] + cost[t];
			if(cap[t] > 0 && nw<dst[y]){
				dst[y] = nw; pre[y] = x; pret[y] = t; que.push(y);
			}
		}
	}
	return dst[T]!=INF;
}
// returns the maximum flow and the minimum cost for this flow
pair<ll,ll> solve(){
	ll totw = 0, totf = 0;
	while(spfa()){
		int minflow = INF;
		for (int x = T; x!=S; x = pre[x]){
			minflow = min(minflow, cap[pret[x]]);
		}
		for (int x = T; x!=S; x = pre[x]){
			cap[pret[x]] -= minflow;
			cap[pret[x]^1] += minflow;
		}
		totf += minflow;
		totw += minflow*dst[T];
		//cout<<minflow<<endl;
	}
	return make_pair(totf, totw);
}
int main(){
	ios::sync_with_stdio(false);cin.tie(0);
	int cases;
	cin>>cases;
	while(cases--){
		cin>>n;
		FN=4*n+15;
		FM=0;
		for(int i=0;i<FN;i++)ra[i].clear();
		addedge(0,1,2,0);
		for(int i=1,u,v,w;i<n;i++){
			cin>>u>>v>>w;
			addedge(u,v,w,1);
			addedge(v,u,w,1);
		}
		cin>>S>>T;
		cout<<solve().first<<"\n";
	}
}
