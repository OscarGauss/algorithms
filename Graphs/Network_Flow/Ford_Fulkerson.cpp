#include <bits/stdc++.h>

using namespace std;

vector<int> AdjList[5050];
long long MRes[5050][5050];
bitset<5050> Vis;
int Source, Sink, f;

void FlowPath(long long minedge, int u=Source){
	Vis[u]=1;
	if(u==Sink){ f=minedge; return ; }
	for(int i=0; i<AdjList[u].size(); i++){
		int v=AdjList[u][i];
		if(MRes[u][v]>0 and !Vis[v])
			FlowPath(min(minedge, MRes[u][v]), v);
		if(f!=INT_MAX){
			MRes[u][v]-=f;
			MRes[v][u]+=f;
			break;
		}
	}	
}

long long MaxFlowFF(int s, int t){
	int mf=0;
	Source=s; Sink=t;
	while(1){
		Vis.reset(); f=INT_MAX;
		FlowPath(INT_MAX);
		if(f==INT_MAX) break;
		mf+=f;
	}
	return mf;
}

int main(){
	int a, b, n, m;
	long long w;
	while(scanf("%d %d\n", &n, &m)!=EOF){
		memset(MRes, 0, sizeof MRes);
		for(int i=0; i<n; i++) AdjList[i].clear();
		while(m--){
			scanf("%d %d %lld\n", &a, &b, &w); a--; b--;
			AdjList[a].push_back(b);
			AdjList[b].push_back(a);
			MRes[a][b]+=w;
			MRes[b][a]+=w;
		}
		long long R=MaxFlowFF(0, n-1);
		printf("%lld\n", R);
	}
	return 0;	
}
