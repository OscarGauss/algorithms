typedef pair<int,int> ii;

void Dijkstra(vector<ii> AdjList[],int &vertices, int start, int Dist[], int Parent[]){
	for(int i=0; i<vertices; i++){
		Dist[i]=(1<<30);
		Parent[i]=-1;
	}
	Dist[start] = 0;
	priority_queue<ii, vector<ii>, greater<ii> > pq;
	pq.push( ii(0,start) );
	while(!pq.empty()){
		ii front = pq.top(); pq.pop();
		int d=front.first, u=front.second;
		if(d>Dist[u]) continue;
		for(int j=0; j<(int)AdjList[u].size(); j++){
			int v=AdjList[u][j].first, w=AdjList[u][j].second;
			if(Dist[v]>Dist[u]+w){ //relax
				Dist[v] = Dist[u]+w;
				Parent[v] = u;
				pq.push(ii(Dist[v],v));
			}//this variant can cause duplicate items in the priority queue
		}
	}
}
