 #include "bits/stdc++.h"

using namespace std;

typedef pair<int,int> ii;
void Dijkstra(vector<ii> AdjList[],int &vertices, int start, int Dist[], int Parent[]){
	for(int i=0; i<vertices; i++){
		Dist[i]=(1<<30);
		Parent[i]=-1;
	}
	Dist[start] = 0;
	priority_queue<ii, vector<ii>, greater<ii> > pq;
	pq.push( ii(0,start) );
	while(!pq.empty()){
		ii front = pq.top(); pq.pop();
		int d=front.first, u=front.second;
		if(d>Dist[u]) continue;
		for(int j=0; j<(int)AdjList[u].size(); j++){
			int v=AdjList[u][j].first, w=AdjList[u][j].second;
			if(Dist[v]>Dist[u]+w){ //relax
				Dist[v] = Dist[u]+w;
				Parent[v] = u;
				pq.push(ii(Dist[v],v));
			}//this variant can cause duplicate items in the priority queue
		}
	}
}
vector<ii> G[100010];
int D[100010];
int P[100010];

int main(){
	int t, vertices, edges, start, u, v, w;
	while(scanf("%d %d\n", &vertices, &edges)!=EOF){		
		for(int i=0; i<vertices; i++) G[i].clear();
		for(int i=0; i<edges; i++){
			scanf("%d %d %d", &u, &v, &w);
			G[u].push_back( ii(v, w) );
		}
		start=2;
		Dijkstra(G, vertices, start, D, P);
		for(int i=0; i<vertices; i++)
			printf("%d ", D[i]);
		printf("\n");
	}
	return 0;
}
