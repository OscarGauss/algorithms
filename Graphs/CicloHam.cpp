#include <bits/stdc++.h>
 
using namespace std;
char M[20][1100100];
vector<int> AdjList[25];
int s;
bool CH(int u, int mask){
    if(mask==0){
        for(int i=0;i<AdjList[u].size();i++)
            if(s==AdjList[u][i])return 1;
        return 0;
    }
    if(M[u][mask]!=-1)return M[u][mask];
    int v;
    for(int i=0;i<AdjList[u].size();i++){
        v=AdjList[u][i];
        if(mask&(1<<v)){          
            if(CH(v,mask^(1<<v)))
                return M[u][mask]=1;
        }
    }
    return M[u][mask]=0;
}
int main(){
    int n, m, u, v, SW;
    char G[21][21]; // por que repiten arcos :/
    char Deg[100];
    while(scanf("%d %d", &n, &m)!=EOF){
        memset(G, 0, sizeof G);
        for(int i=0; i<n; i++) AdjList[i].clear();
        while(m--){
            scanf("%d %d", &u, &v);
            AdjList[u].push_back(v);
            AdjList[v].push_back(u);
            G[u][v]=1; G[v][u]=1;
        }
        for(int i=0; i<n; i++){
            Deg[i]=0;
            for(int j=0; j<n; j++)
                Deg[i]+=G[i][j];
        }
        SW=1;
        for(int u=0; u<n; u++)
            for(int v=0; v<n; v++){
                if(u==v)continue;
                if(!G[u][v] and Deg[u]+Deg[v]<n) SW=0;
            }
        if(n==1) SW=1;
        if(!SW){
            int mask=(1<<n);
            mask--;
            for(int i=0;i<n;i++)
                for(int j=0;j<=mask;j++)
                    M[i][j]=-1;
            for(int i=0;i<n;i++){
                s=i;
                if(CH(i,mask^(1<<i))){
                    SW=1; break;
                }                   
            }
        }               
        printf("%d\n", SW);
    }
    return 0;
} 
