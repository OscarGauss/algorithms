#include "bits/stdc++.h"

using namespace std;

int main(){
	int nodes,edges,a,b,weight;
	while(scanf("%d %d",&nodes,&edges)!=EOF){
		//memset
		int AdjMatrix[nodes+1][nodes+1];
		for(int i=0; i<=nodes; i++)
			for(int j=0; j<=nodes; j++)
				AdjMatrix[i][j]=100000;
		//scan
		for(int i=0; i<edges; i++){
			scanf("%d %d %d",&a,&b,&weight);
			AdjMatrix[a][b]=AdjMatrix[b][a]=weight;
		}
		//Floyd-Warshall
		for(int k=1; k<=nodes; k++){
			for(int i=1;i<=nodes; i++){
				for(int j=1; j<=nodes; j++){
					// Using a array boolean AdjMatrix[i][j] |= (AdjMatrix[i][k] & AdjMatrix[k][j]);
					AdjMatrix[i][j] = min(AdjMatrix[i][j],AdjMatrix[i][k]+AdjMatrix[k][j]);
				}
			}
		}
		//print
		for(int i=1; i<=nodes; i++){
			for(int j=1; j<=nodes; j++){
				printf("%d ",AdjMatrix[i][j]);
			}printf("\n");
		}
	}
	return 0;
}