#include <bits/stdc++.h>

using namespace std;

int Match[100010], Vis[100010];
vector<int> AdjList[100010];

int Aug(int le){
	if(Vis[le]) return 0;
	Vis[le]=1;
	for(int j=0; j<(int)AdjList[le].size(); j++){
		int ri=AdjList[le][j];
		if(Match[ri]==-1 or Aug(Match[ri])){
			Match[ri]=le;
			Match[le]=ri;
			return 1;
		}
	}
	return 0;
}

int main(){
	int t, L, R, x, T;
	scanf("%d", &T);
	for(int t=1; t<=T; t++){
		scanf("%d %d", &L, &R);
		for(int i=0; i<L+R; i++) AdjList[i].clear();
		for(int i=0; i<L; i++){
			for(int j=L; j<R+L; j++){
				scanf("%d", &x);
				if(x){
					AdjList[i].push_back(j);
					AdjList[j].push_back(i);
				}
			}
		}
		int MCBM=0;
		memset(Match, -1, (L+R)*4);
		for(int l=0; l<L; l++){
			for(int j=0; j<(int)AdjList[l].size(); j++){
				int r=AdjList[l][j];
				if(Match[r]==-1){
					Match[l]=r;
					Match[r]=l;
					MCBM++;
					break;
				}
			}
		}
		for(int l=0; l<L; l++){
			if(Match[l]==-1){
				memset(Vis, 0, L*4);
				MCBM+=Aug(l);
			}
		}
		printf("Case %d: a maximum of %d nuts and bolts can be fitted together\n", t, MCBM);
	}
	return 0;
}
