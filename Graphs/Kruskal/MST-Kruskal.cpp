#include "bits/stdc++.h"

using namespace std;
typedef pair<int,int> ii;
typedef pair<int,ii> iii;

vector<int> Parent,Rank,Sizes;

void InitSet(int n){
    Parent.assign(n, 0); Rank.assign(n, 0); Sizes.assign(n, 1);
    for(int i=0; i<n; i++) Parent[i] = i;
}

int Find(int i){
    return Parent[i]==i ? i : Parent[i] = Find(Parent[i]);
}

bool IsSameSet(int i, int j){
    return Find(i) == Find(j);
}

void Union(int i, int j){
    if(!IsSameSet(i, j)){
        int x = Find(i), y = Find(j);
        if(Rank[x] > Rank[y]){
            Parent[y] = x; Sizes[x] += Sizes[y]; 
        }else{
            Parent[x] = y; Sizes[y] += Sizes[x];
            if(Rank[x] == Rank[y])Rank[y]++;
        }
    }
}
//O(Sorting + each edge x cost Union-Find ) O(ElgE+E(~1))=O(ElgE)=O(ElgV^2)=O(2ElgV)=O(ElgV)
int MST_Kruskal(vector<iii> EdgeList, int vertices){
    sort(EdgeList.begin(), EdgeList.end());
    InitSet(vertices);
    int mst=0,t=1;
    for(int i=0; i<EdgeList.size(); i++){
        int u=EdgeList[i].second.first, v=EdgeList[i].second.second, w=EdgeList[i].first;
        if(!IsSameSet(u, v)){
            Union(u, v);
            mst+=w;t++;
        }
        if(t==vertices)break;
    }
    return mst;
}
int main(){
    int vertices, edges, u, v, w;
    vector<iii> EdgeList;
    while(scanf("%d %d",&vertices, &edges)!=EOF){
        EdgeList.clear();
        for(int i=0; i<edges; i++){
            scanf("%d %d %d", &u, &v, &w);
            EdgeList.push_back( iii(w, ii(u-1,v-1) ) );
        }
        printf("%d\n", MST_Kruskal(EdgeList, vertices));
    }
    return 0;
}
