#include <bits/stdc++.h>

using namespace std;
typedef pair<int,int> ii;
typedef pair<int, ii> iii;

int Parent[1000000], Num_sets;

void Init(int n){
    for(int i=0; i<n; i++) Parent[i] = i;
}

int Find(int i){
    return Parent[i]==i ? i : Parent[i] = Find(Parent[i]);
}

bool IsSameSet(int i, int j){
    return Find(i) == Find(j);
}

void Union(int i, int j){
    if(! IsSameSet(i, j) ) Parent[Find(i)]=Find(j);
}

int Kruskal_MST(vector<pair<int,ii> > List, int nodes){
	sort(List.begin(),List.end());
	Init(nodes);
	int mst=0, t=1;
	for(int i=0; i<List.size(); i++){
		int a=List[i].second.first, b=List[i].second.second, w=List[i].first;		if(!IsSameSet(a,b)){
			Union(a,b);
			mst+=w; t++;
		}
		if(t==nodes)break;
	}
	return mst;
}

int main(){
	int nodes,edges,t,lol,a,b,w;
	vector<iii> List;
	while(scanf("%d",&nodes)!=EOF){
		List.clear();
		scanf("%d",&edges);
		while(edges--){
			scanf("%d %d %d",&a, &b, &w);
			List.push_back(iii(w, ii(a, b)));
		}
		printf("%d\n", Kruskal_MST(List,nodes));
	}
	return 0;
}
