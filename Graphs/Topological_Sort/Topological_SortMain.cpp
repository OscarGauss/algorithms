#include <bits/stdc++.h>

using namespace std;

bool isDAG(int n, vector<int> AdjList[], vector<int> &Toposort, int DegIn[]){
    memset(DegIn, 0, n*4); // Kahn algorithm
    for(int u=0; u<n; u++){
        for(int i=0; i<AdjList[u].size(); i++){
            int v=AdjList[u][i]; // u -> v
            DegIn[v]++;
        }
    }
    queue<int> Q;
    for(int u=0; u<n; u++){
        if(!DegIn[u]){
            Q.push(u);
        }
    }
    Toposort.clear();
    while(!Q.empty()){
        int u=Q.front(); Q.pop();
        Toposort.push_back(u);
        for(int i=0; i<AdjList[u].size(); i++){
            int v=AdjList[u][i];
            DegIn[v]--;
            if(!DegIn[v]) Q.push(v);
        }
    }
    if(n==Toposort.size()) return true;
    else return false;
}

vector<int> AdjList[20020];
int DegIn[20020];
vector<int> Toposort;

int main(){
    int n, m, a, b;
    while(scanf("%d %d", &n, &m)!=EOF){
        for(int i=0; i<n; i++) AdjList[i].clear();
		while(m--){
			scanf("%d %d", &a, &b); a--; b--;
			AdjList[a].push_back(b);
		}
		if(isDAG(n, AdjList, Toposort, DegIn)){
            for(int i=0; i<Toposort.size(); i++){
				printf("%d ", Toposort[i]+1);
            }printf("\n");
        }else{
			printf("El Grafo tiene ciclos :(\n");
        }
    }
    return 0;
}
