bool isDAG(int n, vector<int> AdjList[], vector<int> &Toposort, int DegIn[]){
    memset(DegIn, 0, n*4);
    for(int u=0; u<n; u++){
        for(int i=0; i<AdjList[u].size(); i++){
            int v=AdjList[u][i]; // u -> v
            DegIn[v]++;
        }
    }
    queue<int> Q;
    for(int u=0; u<n; u++){
        if(!DegIn[u]){
            Q.push(u);
        }
    }
    Toposort.clear();
    while(!Q.empty()){
        int u=Q.front(); Q.pop();
        Toposort.push_back(u);
        for(int i=0; i<AdjList[u].size(); i++){
            int v=AdjList[u][i];
            DegIn[v]--;
            if(!DegIn[v]) Q.push(v);
        }
    }
    if(n==Toposort.size()) return true;
    else return false;
}