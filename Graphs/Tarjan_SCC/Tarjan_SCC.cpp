#include <bits/stdc++.h>

using namespace std;

#define DBG(a) cout<<#a<<":"<<a<<endl;

int wcc;
vector<int> AdjList[20020];
int Scc[20020], NScc, TNum[20020], TLow[20020], idx;
bool InStack[20020];
stack<int> S;

void TarjanScc(int u){
	TNum[u]=TLow[u]=idx++;
	InStack[u]=1; S.push(u);
	for(int i=0; i<AdjList[u].size(); i++){
		int v=AdjList[u][i];
		if(TNum[v]==-1) // si no esta visitado lo visito
			TarjanScc(v);			
		if(InStack[v]) // si esta en la pila no tiene scc y nos interesa, si no esta en la pila significa que ya tiene scc
			TLow[u]=min(TLow[u], TLow[v]);
	}
	if(TNum[u]==TLow[u]){
		int v;
		do{
			v=S.top(); S.pop(); InStack[v]=0;
			Scc[v]=NScc;
		}while(u!=v);
		NScc++;
	}
}

int main(){
	int n, m, a, b;
	while(scanf("%d %d", &n, &m)!=EOF){		
		for(int i=0; i<n; i++) AdjList[i].clear();
		for(; m--;){
			scanf("%d %d", &a, &b); a--; b--;
			AdjList[a].push_back(b);
		}
		memset(InStack, 0, n);
		memset(TNum, -1, 4*n);
		memset(Scc, -1, 4*n);
		NScc=0; idx=0;
		for(int i=0; i<n; i++){			   
			if(Scc[i]==-1){
				TarjanScc(i);
			}
		}
		for(int i=0; i<n; i++){
			printf("Nodo %d pertenece al componente %d\n", i+1, Scc[i]);
		}				
	}
	return 0;
}
