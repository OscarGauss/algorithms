#include <bits/stdc++.h>

using namespace std;

#define mp make_pair
#define mt make_tuple
#define pb push_back
#define eb emplace_back
#define all(c) (c).begin(),(c).end()
#define forn(i, n) for (int i = 0; i < (n); i++)
typedef long long ll;
vector<int> Color;
bool IsBipartite(vector<vector<int> > AdjList){
    Color.assign(AdjList.size(), -1);
    queue<int> Q;
    for(int i=0; i<AdjList.size(); ++i)
        if(Color[i]==-1){
            Q.push(i); Color[i]=0;
            while(!Q.empty()){
                int u=Q.front(); Q.pop();
                for(int j=0; j<AdjList[u].size(); ++j){
                    if(Color[AdjList[u][j]]==Color[u]) return false;
                    if(Color[AdjList[u][j]]==-1){
                        Color[AdjList[u][j]]=1-Color[u];
                        Q.push(AdjList[u][j]);
                    }
                }
            }
        }
    return true;
}
ll cnt, R, A, B;
vector<vector<int> > AdjList;
vector<int> Vis, Vis1, Degrees;

void dfs(int u){
    cnt++; Vis[u]=1;
    A+=Color[u];
    for(int i=0; i<AdjList[u].size(); i++){
        if(!Vis[AdjList[u][i]]){
            dfs(AdjList[u][i]);
        }
    }
}
void dfs1(int u){
    R+=(cnt-Degrees[u]-1); Vis1[u]=1;
    for(int i=0; i<AdjList[u].size(); i++){
        if(!Vis1[AdjList[u][i]]){
            dfs1(AdjList[u][i]);
        }
    }
}
int main(){
    ll n, m;
    int a, b;
    while(scanf("%I64d %I64d", &n, &m)!=EOF){
        AdjList.assign(n, vector<int> ());
        Degrees.assign(n, 0);
        for(int i=0; i<m; i++){
            scanf("%d %d", &a, &b); --a, --b;
            Degrees[a]++; Degrees[b]++;
            AdjList[a].push_back(b);
            AdjList[b].push_back(a);
        }
        if(!m){
            printf("3 %I64d\n", (n*(n-1)*(n-2))/6); continue;
        }
        if(IsBipartite(AdjList)){
            Vis.assign(n, 0);
            Vis1.assign(n, 0);
            R=0;
            for(int i=0; i<n; i++){
                if(!Vis[i]){
                    cnt=0; A=0;
                    dfs(i);
                    R+=A*(A-1)/2;
//                  cout<<cnt<<" "<<A<<endl;
                    B=cnt-A;
                    R+=B*(B-1)/2;
                }
            }
            if(!R){
                printf("2 %I64d\n", (n-2)*(m)); continue;
            }
            printf("1 %I64d\n", R);
//          cout<<(R>>1)<<endl;
        }else{
            printf("0 1\n");
        }
    }
    return 0;  
} 
