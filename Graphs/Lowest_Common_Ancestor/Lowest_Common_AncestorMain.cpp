#include "bits/stdc++.h"	

using namespace std;

const int MAX_N = 500000;

void StInit(int n, int A[], int St[][MAX_N*2]){
	for(int j=0; j<n; j++) St[0][j]=j;
	for(int i=1; (1<<i )<=n; i++)
		for(int j=0; j+(1<<i)<=n; j++){
			int a=St[i-1][j]; // primera mitad
			int b=St[i-1][j+(1<<(i-1))]; // segunda mitad
			if( A[a]<A[b]) St[i][j]=a;
			else St[i][j]=b;
			
		}
}

int StQuery(int I, int J, int A[], int St[][MAX_N*2]){
	int i=32-__builtin_clz(J-I+1); i--;
	if( A[ St[i][I] ] < A[ St[i][ J-(1<<i)+1 ] ] )
		return St[i][I];
	return St[i][J-(1<<i)+1];
}

vector<int> AdjList[MAX_N];
int St[25][MAX_N*2];
int L[MAX_N*2], E[MAX_N*2], H[MAX_N];
bool Visited[MAX_N];
int idx;

void  dfs(int cur, int depth){
	H[cur] = idx;
	E[idx] = cur;
	L[idx++] = depth;
	Visited[cur]=1;
	for(int i=0; i<AdjList[cur].size(); i++){
		if(!Visited[AdjList[cur][i]]){
			dfs(AdjList[cur][i], depth+1);
			E[idx] = cur;
			L[idx++] = depth;
		}	
	}
}

void BuildLCA(int n, int root){
	idx = 1;
	memset(Visited, 0, n);
	dfs(root, 0); //  E and L size is 2*n ; H size is n dfs(root, depth)
	StInit(n, L, St);
}

int LCA(int u, int v){
	return E[ StQuery(min(H[u], H[v]), max(H[u], H[v]), L, St) ];
}

int main(){
	int n, m, a, b, q, t;
	scanf("%d", &t);
	for(int lol=1; lol<=t; lol++){
		scanf("%d", &n);
		for(int i=0; i<n; i++) AdjList[i].clear();
		for(int i=0; i<n; i++){
			scanf("%d", &m);
			while(m--){
				scanf("%d", &a); a--;
				AdjList[i].push_back(a);
				AdjList[a].push_back(i);
			}
		}
		BuildLCA(2*n, 0);
		scanf("%d", &q);
		printf("Case %d:\n", lol);
		while(q--){
			scanf("%d %d", &a, &b); a--; b--;
			printf("%d\n", LCA(a, b)+1 );
		}
	}
	return 0;
}
