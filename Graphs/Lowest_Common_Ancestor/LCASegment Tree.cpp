#include "bits/stdc++.h"	

using namespace std;

#define Right(n)  ( (n<<1)    )
#define Left(n)   ( (n<<1) +1 )
#define MidL(i,j) ( ( (i+j)>>1 )    )
#define MidR(i,j) ( ( (i+j)>>1 ) +1 )
//LCA para 500000 nodos
vector<vector<int> > AdjList;
vector<int> Tree(1048576+2); // 2*{2^[floor(log2 n)+1]} 100000:262144,500000:1048576
vector<int> L(1000000), E(1000000), H(500000);
vector<bool> Visited(500000);
int idx;

void BuildRMQ(int node,int p,int q){
	if(p==q){ Tree[node] = p; return ; }
	BuildRMQ(Left(node) , p, MidL(p,q));
	BuildRMQ(Right(node), MidR(p,q), q);
	int p1 = Tree[Left(node)];
	int p2 = Tree[Right(node)];
	Tree[node] = (L[p1]<= L[p2]) ? p1 : p2;
}

int RMQ(int node, int p,int q, int l, int r){
	if(l > q or r < p ) return -1; //l,r fuera de rango  cuando l>q or r<p
	if(p >= l and q <= r) return Tree[node];
	int p1 = RMQ(Left(node), p, MidL(p,q), l, r);
	int p2 = RMQ(Right(node), MidR(p,q), q, l, r);
	if(p1==-1) return p2;
	if(p2==-1) return p1;
	return (L[p1]<= L[p2]) ? p1 : p2;
}

void  dfs(int cur, int depth){
	H[cur] = idx;
	E[idx] = cur;
	L[idx++] = depth;
	Visited[cur]=1;
	for(int i=0; i<AdjList[cur].size(); i++){
		if(!Visited[AdjList[cur][i]]){
			dfs(AdjList[cur][i], depth+1);
			E[idx] = cur;
			L[idx++] = depth;
		}	
	}
}

void BuildLCA(int n){
	idx = 1;
	fill_n(Visited.begin(), n+1, false);
	dfs(1, 0);//  E and L size is 2*n ; H size is n
	BuildRMQ(1, 1, n);
}

int LCA(int u, int v, int n){
	return E[ RMQ(1, 1, n, min(H[u], H[v]), max(H[u], H[v])) ];
}

int main(){
	int n, m, a, b, q;
	while(scanf("%d %d", &n, &m)!=EOF){
		AdjList.assign(n+1, vector<int>() );
		while(m--){
			scanf("%d %d", &a, &b);
			AdjList[a].push_back(b);
			AdjList[b].push_back(a);
		}
		BuildLCA((2*n));
		scanf("%d", &q);
		while(q--){
			scanf("%d %d", &a, &b);
			printf("LCA(%d,%d) = %d\n", a, b, LCA(a, b, (2*n)) );
		}
	}
	return 0;
}

/*
10 9
2 1
3 2
4 2
7 2
5 4
6 4
8 1
9 8
10 8
10
5 6
5 7
5 2
9 10
5 9
1 1
10 10
10 1
5 9
9 5

Order
10 9
1 2
1 8
2 3
2 4
2 7
4 5
4 6
8 9
8 10
*/