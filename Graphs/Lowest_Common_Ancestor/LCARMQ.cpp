vector<vector<int> > AdjList;
int L[1000000], E[1000000], H[500000];
bool Visited[500000];
int idx;

void  dfs(int cur, int depth){
	H[cur] = idx;
	E[idx] = cur;
	L[idx++] = depth;
	Visited[cur]=1;
	for(int i=0; i<AdjList[cur].size(); i++){
		if(!Visited[AdjList[cur][i]]){
			dfs(AdjList[cur][i], depth+1);
			E[idx] = cur;
			L[idx++] = depth;
		}	
	}
}

void BuildLCA(int n, int root){
	idx = 1;
	memset(Visited, 0, 2*n);
	dfs(root, 0); //  E and L size is 2*n ; H size is n dfs(root, depth)
	BuildRMQ(2*n, L);
}

int LCA(int u, int v){
	return E[ RMQ(min(H[u], H[v]), max(H[u], H[v]), L, St) ];
}