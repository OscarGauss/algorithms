#include "bits/stdc++.h"	

using namespace std;

void StInit(int n, int A[], int St[][1000010]){ }
int StQuery(int I, int J, int A[], int St[][1000010]){ }

//LCA para 500000 nodos

vector<vector<int> > AdjList;
int St[25][1000010];
int L[1000000], E[1000000], H[500000];
bool Visited[500000];
int idx;

void  dfs(int cur, int depth){
	H[cur] = idx;
	E[idx] = cur;
	L[idx++] = depth;
	Visited[cur]=1;
	for(int i=0; i<AdjList[cur].size(); i++){
		if(!Visited[AdjList[cur][i]]){
			dfs(AdjList[cur][i], depth+1);
			E[idx] = cur;
			L[idx++] = depth;
		}	
	}
}

void BuildLCA(int n, int root){
	idx = 1;
	memset(Visited, 0, n);
	dfs(root, 0); //  E and L size is 2*n ; H size is n dfs(root, depth)
	StInit(n, L, St);
}

int LCA(int u, int v){
	return E[ StQuery(min(H[u], H[v]), max(H[u], H[v]), L, St) ];
}

int main(){
	int n, m, a, b, q, t;
	scanf("%d", &t);
	for(int lol=1; lol<=t; lol++){
		scanf("%d", &n);
		AdjList.assign(n, vector<int>() );
		for(int i=0; i<n; i++){
			scanf("%d", &m);
			while(m--){
				scanf("%d", &a); a--;
				AdjList[i].push_back(a);
				AdjList[a].push_back(i);
			}
		}
		BuildLCA(2*n, 0);
		scanf("%d", &q);
		printf("Case %d:\n", lol);
		while(q--){
			scanf("%d %d", &a, &b); a--; b--;
			printf("%d\n", LCA(a, b)+1 );
		}
	}
	return 0;
}
