#include <bits/stdc++.h>
using namespace std;

vector<int> G[100100];
int Path[100100];
int N, M;

bool dfs(int u, int p = -1) {
  int cnt = 0;
  Path[u] = u;
  for (int i = 0; i < G[u].size(); i++) {  // escojo dos aristas
    int v = G[u][i];
    if (v == p) continue;
    if (dfs(v, u) and cnt < 2) {  // guardo quien es mi padre del path
      Path[v] = u;
      cnt++;
    }
  }
  return cnt < 2;
}

int MPCT() {
  memset(Path, -1, N * 4);
  for (int i = 0; i < N; i++)
    if (Path[i] == -1) dfs(i);
  int path = 0;
  for (int i = 0; i < N; i++)
    if (Path[i] == i) path++;
  return path;
}

int main() {
  int test, u, v;
  scanf("%d\n", &test);
  while (test--) {
    scanf("%d %d\n", &N, &M);
    // scanf("%d\n", &N); M=N-1;
    for (int i = 0; i < N; i++) G[i].clear();
    while (M--) {
      scanf("%d %d\n", &u, &v);
      u--; v--;
      G[u].push_back(v);
      G[v].push_back(u);
    }
    printf("%d\n", N + MPCT() - 1);
  }
  return 0;
}
