#include <bits/stdc++.h>
using namespace std;

vector<int> G[100100];
vector<int> T[100100];
int Path[100100], Deg[100100], path;
void dfs(int u){
	Path[u]=path;
	for(int i=0; i<T[u].size(); i++)
		if(Path[T[u][i]]==-1)
			dfs(T[u][i]);
}

int main(){
	int test, a, b, n, m;
	scanf("%d\n", &test);
	deque<int> V;
	while(test--){
		scanf("%d %d\n", &n, &m);
		//scanf("%d\n", &n); m=n-1; 
		for(int i=0; i<n; i++){ G[i].clear(); T[i].clear(); }
		memset(Deg, 0, n*4);
		for(int i=0; i<m; i++){
			scanf("%d %d\n", &a, &b); a--; b--;
			G[a].push_back(b);
			G[b].push_back(a);
			Deg[a]++; Deg[b]++;
		}
		V.clear();
		memset(Path, 0, n*4);
		int top=0;
		for(int i=0; i<n; i++)
			if(!Path[i]){
				Path[i]=1;
				V.push_back(i);
				while(top<V.size()){
					int u=V[top]; top++;
					for(int i=0; i<G[u].size(); i++){
						int v=G[u][i];
						if(!Path[v]){
							Path[v]=1;
							T[u].push_back(v);
							V.push_back(v);
						}
					}
				}
			}
		for(int i=0; i<V.size(); i++){ cout<<V[i]<<" "; }cout<<endl;
		memset(Path, -1, n*4);
		path=0;
		for(int i=n-1; i>=0; i--){
			int u=V[i];
			if(Deg[u]<3) continue;
			int x=-1, y=-1; // Elegir dos hijos
			for(int j=0; j<T[u].size() and y==-1; j++){
				int v=T[u][j];
				if(Path[v]==-1)
					if(x==-1)x=v;
					else y=v;
			}
			Path[u]=path;
			dfs(x); dfs(y);
			path++;
			for(int j=0; j<G[u].size(); j++)
				if(Path[G[u][j]]==-1)
					Deg[G[u][j]]--;
			Deg[u]=2;
		}
		for(int i=0; i<n; i++)
			if(Path[V[i]]==-1){
				dfs(V[i]); path++;
			}
		for(int i=0; i<n; i++) cout<<Path[i]<<" "; cout<<endl;
		cout<<"p:"<<path<<endl;
		printf("%d\n", n+path-1);
		
	}
	return 0;
				 
}
