#include <bits/stdc++.h>

using namespace std;

vector<int> AdjList[2020], AdjList2[2020];
deque<int> Aux;
int Scc[2020], Vis[2020], n, scc;

void dfs1(int u){
	Vis[u]=1;
	for(int i=0; i<AdjList[u].size(); i++){
		int v=AdjList[u][i];
		if(Vis[v]==-1)dfs1(v);
	}
	Aux.push_front(u);
}

void dfs2(int u){
	Scc[u]=scc;
	for(int i=0; i<AdjList2[u].size(); i++){
		int v=AdjList2[u][i];
		if(Scc[v]==-1) dfs2(v);
	}
}

void Kosaraju(int n){
	memset(Vis, -1, n*4);
	Aux.clear();
	for(int i=0; i<n; i++)
		if(Vis[i]==-1) dfs1(i);
	scc=0;
	memset(Scc, -1, n*4);
	for(int i=0; i<Aux.size(); i++)
		if(Scc[Aux[i]]==-1){
			dfs2(Aux[i]); scc++;
		}
}

int main(){
	int n, m, a, b;
	while(scanf("%d %d", &n, &m)!=EOF){
		for(int i=0; i<n; i++) AdjList[i].clear();
		for(int i=0; i<n; i++) AdjList2[i].clear();
		for(;m--;){
			scanf("%d %d", &a, &b); a--; b--;
			AdjList[a].push_back(b);
			AdjList2[b].push_back(a);
		}
		Kosaraju(n);		
		for(int i=0; i<n; i++){
			printf("Nodo %d pertenece al componente %d\n", i+1, Scc[i]);
		}
	}
	return 0;
}
