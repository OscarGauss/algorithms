void SPFA(int s, int n){
	Dist.assign(n+1, 1<<30);
	Dist[s]=0;
	queue<int> q;
	q.push(s);
	vector<int> in_queue(n+1,0);
	in_queue[s]=1;
	while(!q.empty()){
		int u=q.front(); q.pop(); in_queue[u]=0;
		for(int i=0; i<AdjList[u].size(); i++){
			int v = AdjList[u][i].first;
			int w_u_v = AdjList[u][i].second;
			if(Dist[u]+w_u_v< Dist[v]){
				Dist[v] = Dist[u]+w_u_v;
				if(!in_queue[v]){
					q.push(v);
					in_queue[v]=1;
				}
			}
		}
	}
}