#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
ll P[12]={2,3,5,7,11,13,17,19,23,29,31,37};
ll mulm(ll a, ll b, ll mod){
	ll x=0;a=a%mod;
	while(b>0){
		if(b&1)x=(x+a)%mod;
		a=(2*a)%mod;
		b/=2;
	}
	return x%mod;
}
ll POW(ll a, ll b, ll mod){
	if(b==0)return 1;
	return mulm(POW(mulm(a,a,mod),b/2,mod),(b&1?a:1),mod) ;
}
bool MillerRabin(ll x){
	if(x<=40){
		for(int i=0;i<12;++i)if(P[i]==x)return 1;
		return 0;
	}
	if(x%2==0)return 0;
	ll s=0,d=x-1;
	while(d%2==0){
		d/=2;
		s++;
	}
	ll a,u,j;
	for(int i=0;i<12;i++){
		a=P[i];
		u = POW(a,d,x);
		if(u!=1 and u!=x-1){
			j=1;
			while(u!=x-1 and j<s){
				j++;
				u=mulm(u,u,x);
				if(u==1)
					return 0;
			}			
			if(u!=x-1)return 0;
		}			
	}
	return 1;
}
ll rho(ll n){
	int i=0,k=2;
	ll x=3,y=3,g;
	ll lim=100000;
	while(1){
		i++;
		if(i%lim==0){
			x++;
			y=x;
		}
		x=(POW(x,2,n)+n-1)%n;
		g=__gcd(abs(y-x),n);
		if(g!=1 and g!=n)return g;
		if(i==k){y=x;k*=2;}
	}
}
int main(){	
	ll n,f;
	while(cin>>n){
		ll u,f;
	    queue<ll>Q;
		vector<ll> A;
		Q.push(n);
		while(!Q.empty()){
			u=Q.front();
			Q.pop();
			if(MillerRabin(u))
				A.push_back(u);
			else{
				f=rho(u);
				Q.push(f);
				Q.push(u/f);
			}		
		}
		sort(A.begin(),A.end());
		ll r=1,c=1;
		for(int i=1;i<A.size();i++)
			if(A[i]!=A[i-1]){
				r*=(c+1);
				c=1;
			}else c++;
		
		cout<<r*(c+1)<<endl;
	}	
	return 0;
}
