#include <bits/stdc++.h>
using namespace std;
typedef	long long ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;
ll m;
ll mul(ll a, ll b){
	return ((a%m)*(b%m))%m;
}
ll add(ll a, ll b){
	return ((a%m)+(b%m))%m;
}
vvi mulM(vvi A, vvi B){
	vvi C;
	C.assign(2,vi(2));
	C[0][0]= add ( mul( A[0][0],B[0][0]), mul( A[0][1],B[1][0])) ;
	C[0][1]= add ( mul( A[0][0],B[0][1]), mul( A[0][1],B[1][1])) ;
	C[1][0]= add ( mul( A[1][0],B[0][0]), mul( A[1][1],B[1][0])) ;
	C[1][1]= add ( mul( A[1][0],B[0][1]), mul( A[1][1],B[1][1])) ;
	return C;
}
vvi f(vvi A, int n){
	if(n==1)return A;
	if(n&1)return mulM(f(mulM(A,A),n/2),A);
	return f(mulM(A,A),n/2);
}
int main(){
	int n;
	ll a,b;
	int t;
	cin>>t;
	while(t--){
		cin>>a>>b>>n>>m;
		m=pow(10,m);
		
		vvi A;
		//cout<<m<<endl;
		A.assign(2,vi(2));
		A[0][0]=0;
		A[0][1]=1;
		A[1][0]=1;
		A[1][1]=1;
		A = f(A,n);
		cout<<add( mul(A[0][0],a) , mul(A[0][1],b))<<"\n";
	}
	return 0;
}