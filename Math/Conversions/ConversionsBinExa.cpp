#include <iostream>
#include <cmath>

using namespace std;

long long NumBin (long long n){
    long long aux,numbin=0,d,cb=0;
    for(int j=0;n>0;j++){
        d=n%2;
        //numbin=numbin+pow(10,j)*d;
        cb+=d;
        n/=2;
    }
    //cout<<cb<<" "<<numbin<<endl;
    return cb;
}
long long NumHex (long long n){
    long long aux=0,numhex=0,d;
    for(int j=0;n>0;j++){
        d=n%16;
        if (d<9){
            numhex=numhex+pow(10,j)*d;
        }else{
            numhex=numhex+pow(10,j)*(d-6);
        }
        n/=16;
        //cout<<numhex<<endl;
    }
    return numhex;
}

int main()
{
    long long n,x,hex,X1,X2;
    cin>>n;
    for(int i=0;i<n;i++){
        cin>>x;
        hex=NumHex(x);
        cout<<NumBin(x)<<" "<<NumBin(hex)<<endl;
    }
    return 0;
}
