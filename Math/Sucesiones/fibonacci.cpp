#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

int memo[10000];

int f(int n){
    if(memo[n]==-1)
        if(n<=1)
            memo[n]=n;
        else
            memo[n]=f(n-1)+f(n-2);

    return memo[n];
}

//Solo con dos esoacios de memoria
int fi(int n){
    int mem[2];
    mem[0]=0;
    mem[1]=1;
    for(int i=2;i<=n;i++){
        mem[i&1] = mem[ (i%2==0)?1:0 ]+mem[i&1];
    }
    return mem[n&1];
}

int main(){
    long long n;
    memset(memo,-1,sizeof memo );
    while(scanf("%lld",&n)){
        printf("%d\n", f(n));
        printf("%d\n", fi(n));
    }
    return 0;
}
