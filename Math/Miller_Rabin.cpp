#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
ll P[12]={2,3,5,7,11,13,17,19,23,29,31,37};
ll mulm(ll a, ll b, ll mod){
	ll x=0;a=a%mod;
	while(b>0){
		if(b&1)x=(x+a)%mod;
		a=(2*a)%mod;
		b/=2;
	}
	return x%mod;
}
ll POW(ll a, ll b, ll mod){
	if(b==0)return 1;
	return mulm(POW(mulm(a,a,mod),b/2,mod),(b&1?a:1),mod) ;
}
bool MillerRabin(ll x){
	if(x<=40){
		for(int i=0;i<12;++i)if(P[i]==x)return 1;
		return 0;
	}
	if(x%2==0)return 0;
	ll s=0,d=x-1;
	while(d%2==0){
		d/=2;
		s++;
	}
	ll a,u,j;
	for(int i=0;i<12;i++){
		a=P[i];
		u = POW(a,d,x);
		if(u!=1 and u!=x-1){
			j=1;
			while(u!=x-1 and j<s){
				j++;
				u=mulm(u,u,x);
				if(u==1)
					return 0;
			}			
			if(u!=x-1)return 0;
		}			
	}
	return 1;
}
int main(){
	ll x;
	while(cin>>x){
		if(MillerRabin(x))cout<<"Prime\n";
		else cout<<"Not prime\n";
	}
	return 0;
}
