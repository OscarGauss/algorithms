#include <bits/stdc++.h>

using namespace std;

const int N = 10000000 ;

bitset<10000010> Sieve;
void FillSieve(){
	Sieve.set(); Sieve[0]=Sieve[1]=0;
	for(int i=4; i<=N; i+=2) Sieve[i]=0;
	for(int i=3; i*i<=N; i+=2)
		if(Sieve[i])
			for(int j=i*i; j<=N; j+=2*i)
				Sieve[j]=0;				
}

int lp [N+1];
vector<int> Primes;
void FillSieveExt(){
	memset(lp, 0, sizeof lp); Primes.clear();
	for(int i=2; i<=N; ++i) {
		if(lp[i]==0){
			lp[i]=i ;
			Primes.push_back(i);
		}
		for(int j=0; j<(int)Primes.size() && Primes[j]<=lp[i] && i*Primes[j]<=N ; ++j)
			lp[i*Primes[j]]=Primes[j];
	}
}

int Factorize(int X, int Factors[]){
	int F=0;
	while(lp[X]>1){
		Factors[F++]=lp[X];
		X/=lp[X];
	}
	return F;
}

int main(){
	FillSieve();
	FillSieveExt();
	int fa[1000], n=Factorize(3224234, fa);
	cout<<3224234<<endl;
	for(int i=0; i<n; i++) cout<<fa[i]<<" "; cout<<endl;
}
