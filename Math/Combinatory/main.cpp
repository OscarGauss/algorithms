/*
* Algoritmos para Combinatoria.
*
* Binomio de Newton.
* Termino medio.
* X Termino.
* Inclusion y exclsion.
* Funci�n de Euler.
* Criba de Erat�stenes.
* Cierre transitivo.
* @author Oscar Gauss Crvajal Yucra C.I. 9895530
* @version 3.0 30/07/13

*/



#include <iostream>
#include <cmath>
#include <algorithm>
#include <cstring>
using namespace std;


int mcd (int a,int  b)
{
    if (b == 0)
        {return a;}
    else { mcd (b, a % b);}
}
bool primo(int n)
{
    if (n<=1)
        return false;

    for (int i=2;i*i<=n;i++)
    {
        if (n%i==0)
        {
            return false;
        }
    }
    return true;
}
void newton(long long v[],int n)
{
    long long i,j;

        for (i=0;i<=n;i++)
        {

        if (i<=1)
        {
                    if (i==0)
                    {
                        v[0]=1;

                    }

                else
                {
                    v[0]=1;
                    v[1]=1;
                }

        }
        else
        {
            v[i-1]=1;
            for (j=i-2;j>0;j--)
            {
                v[j]=v[j]+v[j-1];
            }
            v[0]=1;

        }



        }

}


void nter(int n, int k)
{

    long long v[n+1];
    newton(v,n+1);
    if (v[k-1]!=1)
    {
        cout<<v[k-1];
    }
    if (n-k+1>=1)
    {
        if (n-k+1==1)
        {
            cout<<"a";
        }
        else
        {
            cout<<"a^"<<n-k+1;
        }



    }
    if (k-1>=1)
    {
        if (k-1==1)
        {
            cout<<"b";
        }
        else
        {
             cout<<"b^"<<k-1;
        }



    }
}

void binomio ()
{
    int n,j,i;
    cout<<"Dar un valor para n / (a+b)^n :"<<endl;
    cin>>n;
    long long v[n];
    for (int j=1;j<=n+1;j++)
        {
            newton(v,j);
            for (i=0;i<j;i++)
            {
                cout<<v[i]<<" ";
            }
            cout<<endl;
        }


        cout<<endl;
        cout<<endl;

}
void tmedio ()
{
    int n,k;
     cout<<"Ingrese un un valor para n / (a+b)^n :"<<endl;
     cin>>n;
     if (n%2==0)
     {
         k=n/2+1;
         nter(n,k);
     }
     else
     {
         k=(n+1)/2;
         nter(n,k);
         cout<<" + ";
         k=(n+1)/2;
         nter(n,k);
     }
     cout<<endl<<endl;


}


void xter ()
{
    int k,n;
    cout<<"Ingrese un un valor para n / (a+b)^n :"<<endl;
    cin>>n;
    cout<<"Dtermine el termino buscado"<<endl;
    cin>>k;
    nter(n,k);
    cout<<endl<<endl;



}
void inclusionexclsion()
{
    int i,j,n,k,prop,c,aux,x,st,r;

    cout<<"Determine el tama�o del conjunto A"<<endl;
    cin>>n;
    int v[n];
    int p[n];

    cout<<"Inserte valores para n elementos de A"<<endl;
    cout<<"[A]i   peso de [A]i"<<endl;
    for (i=0;i<n;i++)
    {
        cin>>v[i];
        cin>>p[i];
    }
    cout<<"Ingrese n�mero de propiedades"<<endl;
    cin>>k;
    bool m[k][n];
    cout<<"Determine caracteristicas para las Propiedades "<<endl<<endl;
    cout<<"(1) Subconjunto de numero primos"<<endl;
    cout<<"(2) Subconjunto de cuadrados prefectos"<<endl;
    cout<<"(3) Subconjunto de Multiplos de X"<<endl;

    for (i=0;i<k;i++)
    {
        cout<<"P"<<i+1<<endl;
        si:
        cin>>prop;
        if (prop==3)
        {
            cout<<"introdusca un valor para x = ";
            cin>>x;
        }
        for (j=0;j<n;j++)
        {
            switch (prop)
            {
            case 1:
            if (primo(v[j]))
                {
                    m[i][j]=true;
                }
                else {m[i][j]=false;}
            break;
            case 2:
            aux=sqrt(v[j]);
            if (aux*aux==v[j])
                {
                    m[i][j]=true;
                }
                else {m[i][j]=false;}
            break;
            case 3:
                if (v[j]%x==0)
                {
                    m[i][j]=true;
                }
                else {m[i][j]=false;}
                break;
            default :
                cout<<"Debe introducir un valor del 1 al 3";
                goto si;

            };

        }




    }
    cout<<"Determine la cantidad (r) de propiedades que deben cumplir los elementos"<<endl;
    cin>>r;
    st=0;
    for (i=0;i<n;i++)
    {
        c=0;

        for (j=0;j<k;j++)
        {
            if (m[j][i])
            {
                c++;

            }

        }
        if (c>=r)
        {
            st=st+p[i];
        }
    }
    cout<<"Suma de los pesos de elementos que cumplen "<<r<<" propiedades: "<<st<<endl<<endl<<endl<<endl;

}

void euler ()
{
    cout<<"Ingrese un numero"<<endl;
    int c=0,n,i;
    cin>>n;
    for (i=1;i<=n;i++)
    {
        if (mcd(n,i)==1)
        {
            c++;
        }
    }
    cout<<"Existen "<<c<<" numeros que no son divisibles entre los factores primos de "<<n<<endl<<endl;

}
void eratostenes()
{
    cout<<"Ingrese un rango de numeros [a,b]"<<endl;
    int n,i,j,c=0,a,b;

    cin>>a>>b;


    n=b;
    bool v[n+1];
    memset(v, true, sizeof(v));
    v[0]=false;
    v[1]=false;
    for (i=2;i*i<=n;i++)
    {

        if (v[i])
        {
             for (j=i+i;j<=n;j+=i)
            {
                v[j]=false;
            }
        }

    }
    for (j=a;j<=b;j++)
        {
            if (v[j])
            {
                c++;
            }

        }
    cout<<"Existen "<<c<<" primos en el rango de ["<<a<<","<<b<<"]"<<endl<<endl;



}
bool m[100][100];
bool v[1000];
int main()
{
    bool swww=true;
    principio:
    int op;
    char opp;
    cout<<"Ingrese el numero respectivo"<<endl<<endl;
    cout<<"(A) Binomio de Newton"<<endl;
    cout<<"(B) Termino medio"<<endl;
    cout<<"(C) X Termino"<<endl;
    cout<<"(D) Inclusion y exclsion"<<endl;
    cout<<"(E) Funci�n de Euler"<<endl;
    cout<<"(F) Criba de Erat�stenes"<<endl;
    cout<<"(G) Author"<<endl;
    si:
    cin>>opp;
    //op=(int)opp;
    switch (opp)
    {
    case 'A':binomio();
    break;
    case'B':tmedio();
    break;
    case 'C':xter();
    break;
    case 'D':inclusionexclsion();
    break;
    case 'E':euler();
    break;
    case 'F':eratostenes();
    break;
    case 'G':cout<<endl<<"Author Oscar Gauss Crvajal Yucra C.I. 9895530"<<endl;
    break;
    default: {cout<<"La latra es incorrecta.";
    goto si;
    break;}
    };
     goto principio;





}
