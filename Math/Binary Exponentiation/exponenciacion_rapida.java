
public class exponenciacion_rapida {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(((142%5)));

	}
	static int fastExp(int base, int p) {
		if (p == 0) return 1;
		else if (p == 1) return base;
		else {
			int res = fastExp(base, p / 2); res *= res;
			if (p % 2 == 1) res *= base;
			return res; 
		} 
	}
}
