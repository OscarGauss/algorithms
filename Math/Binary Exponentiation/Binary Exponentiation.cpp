#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

ll BinaryExponentiation(ll base,ll expo){
	if(!expo) return 1;
	ll temp = BinaryExponentiation(base, expo/2);
	temp*=temp;
	if(expo&1) temp*=base;
	return temp;
}

ll BinaryModularExponentation(ll base, ll expo, ll mod){
	if(!expo)return 1;
	ll temp = BinaryModularExponentation(base, expo/2, mod);
	temp%=mod; temp*=temp; temp%=mod;
	if(expo&1){
		temp*=base; temp%=mod;
	}
	return temp;
}

int main(){
	ll base, expo, mod;
	while(scanf("%lld %lld %lld", &base, &expo, &mod)!=EOF){
		printf("%lld\n", BinaryExponentiation(base, expo));
		printf("%lld\n", BinaryModularExponentation(base, expo, mod));
	}
	return 0;
}
