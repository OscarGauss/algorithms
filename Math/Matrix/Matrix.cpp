#include <bits/stdc++.h>

using namespace std;

typedef	long long ll;
typedef vector<ll> vi;
typedef vector<vi> vvi;

void Print_Matrix(vvi A){
	for(int i=0; i<A.size(); i++)
		for(int j=0; j<A[i].size(); j++)
			printf("%lld ", A[i][j]);
		printf("\n");
}

vvi Add_Matrix(vvi A, vvi B){
	if(A.size()!=B.size() || A[0].size()!=B[0].size()) return vvi();
	vvi C(A.size(), vi(A[0].size()));
	for(int i=0; i<C.size(); i++)
		for(int j=0; j<C[i].size(); j++)
			C[i][j]=A[i][j]+B[i][j];
	return C;
}
vvi Multiply_Matrix(vvi A, vvi B){
	if(A[0].size()!=B.size()) return vvi();
	vvi C(A.size(), vi(B[0].size(), 0));
	for(int i=0; i<C.size(); i++)
		for(int j=0; j<C[i].size(); j++)
			for(int k=0; k<B.size(); k++)
				C[i][j]+=A[i][k]*B[k][j];
	return C;
}

vvi Expon_Matrix(vvi A, int exp){
	if(exp==1) return A;
	if(exp&1) return Multiply_Matrix(Expon_Matrix(Multiply_Matrix(A, A), exp>>1), A);
	return Expon_Matrix(Multiply_Matrix(A, A), exp>>1);
}

int main(){
	int n, m, exp;
	while(scanf("%d %d %d", &n, &m, &exp)!=EOF){
		vvi A(n, vi(m));
		for(int i=0; i<n; i++)
			for(int j=0; j<m; j++)
				scanf("%lld", &A[i][j]);
		scanf("%d %d %d", &n, &m, &exp);
		vvi B(n, vi(m));
		for(int i=0; i<n; i++)
			for(int j=0; j<m; j++)
				scanf("%lld", &B[i][j]);
		A=Multiply_Matrix(A, B);
		Print_Matrix(A);
		A=Add_Matrix(A, A);
		Print_Matrix(A);
		A=Expon_Matrix(B, 4);
		Print_Matrix(A);
	}
	return 0;  
}
