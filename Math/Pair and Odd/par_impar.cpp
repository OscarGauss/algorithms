#include <cstdio>

using namespace std;

int main(){
    long long n;
    while(scanf("%lld",&n)){

        ///With & Example 1&1(1 & 1 = 1), 2&1(10 & 1 = 0), 3&1(11 & 1 = 1), 4&1(100 & 1 = 0)
        printf("%d -> ",n&1);
        if(n&1)
            printf("es impar\n");
        else
            printf("es par\n");

        ///With Module
        if(n%2==0)
            printf("es par\n");
        else
            printf("es impar\n");
    }
    return 0;
}
